import React from "react";

export const RightArrowIcon = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={23.319}
      height={11.897}
      viewBox="0 0 23.319 11.897"
      {...props}
    >
      <path
        d="M16.75.541a1.036 1.036 0 00-.008 1.457l2.872 2.88H1.275a1.03 1.03 0 000 2.061h18.331l-2.872 2.88a1.043 1.043 0 00.008 1.458 1.026 1.026 0 001.45-.008l4.583-4.63h0a1.157 1.157 0 00.214-.325.983.983 0 00.079-.4 1.033 1.033 0 00-.293-.716L18.191.567A1.01 1.01 0 0016.75.541z"
        fill="#8a92a3"
        stroke="#fff"
        strokeWidth={0.5}
      />
    </svg>
  );
};

export const ZipZackBg = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={1366}
      height={394}
      viewBox="0 0 1366 394"
      {...props}
    >
      <defs>
        <linearGradient
          id="prefix__a"
          x1={0.5}
          x2={0.5}
          y2={1}
          gradientUnits="objectBoundingBox"
        >
          <stop offset={0} stopColor="#d3d7de" stopOpacity={0} />
          <stop offset={1} stopColor="#c8cdd6" />
        </linearGradient>
      </defs>
      <path d="M0 0h1366v394H0z" fill="url(#prefix__a)" />
    </svg>
  );
};

export const ArrowDown = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={8.435}
      height={5.038}
      viewBox="0 0 8.435 5.038"
      {...props}
    >
      <path
        d="M4.397 4.453l3.407-3.417a.286.286 0 10-.406-.4l-3.2 3.214L.998.636a.286.286 0 00-.405.4l3.395 3.417a.286.286 0 00.405 0z"
        fill="#383b62"
        stroke="#383b62"
      />
    </svg>
  );
};

export const BgAboutUs = (props) => {
  const { children } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={1366}
      height={666}
      viewBox="0 0 1366 666"
      {...props}
    >
      {children}
      <defs>
        <linearGradient
          id="prefix__b"
          x1={0.287}
          y1={0.623}
          x2={1.13}
          y2={0.824}
          gradientUnits="objectBoundingBox"
        >
          <stop offset={0} stopColor="#fafafb" />
          <stop offset={1} stopColor="#fff" />
        </linearGradient>
        <clipPath id="prefix__a">
          <path fill="#d3d7de" d="M0 0h1366v666H0z" />
        </clipPath>
      </defs>

      <g clipPath="url(#prefix__a)" fill="url(#prefix__b)">
        <rect
          width={706}
          height={706}
          rx={34}
          transform="rotate(30 1029.313 1440.196)"
          opacity={0.797}
        />
        <rect
          width={1049}
          height={1049}
          rx={34}
          transform="rotate(30 1506.994 82.374)"
          opacity={0.6}
        />
      </g>
    </svg>
  );
};

export const SmallRightIcon = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={8.414}
      height={14.828}
      viewBox="0 0 8.414 14.828"
      {...props}
    >
      <path
        d="M1.414 13.414l6-6-6-6"
        fill="none"
        stroke="#6966c7"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </svg>
  );
};

export function MobileNextButton(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={100}
      height={100}
      style={{marginRight : "-22px"}}
      viewBox="0 0 52.66 52.66"
      {...props}
    >
      <defs>
        <filter
          id="prefix__a"
          x={0}
          y={0}
          width={52.66}
          height={52.66}
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy={3} />
          <feGaussianBlur stdDeviation={3} result="b" />
          <feFlood floodOpacity={0.161} />
          <feComposite operator="in" in2="b" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g filter="url(#prefix__a)">
        <circle
          cx={17.33}
          cy={17.33}
          r={17.33}
          transform="translate(9 6)"
          fill="#fff"
        />
      </g>
      <path
        d="M25.089 27.43l3.781-3.781-3.781-3.781"
        fill="none"
        stroke="#6966c7"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </svg>
  );
}

export function DesktopNextButton(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={73}
      height={73}
      viewBox="0 0 73 73"
      {...props}
    >
      <defs>
        <filter
          id="prefix__a"
          x={0}
          y={0}
          width={73}
          height={73}
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy={3} />
          <feGaussianBlur stdDeviation={3} result="b" />
          <feFlood floodOpacity={0.161} />
          <feComposite operator="in" in2="b" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g filter="url(#prefix__a)">
        <circle
          cx={27.5}
          cy={27.5}
          r={27.5}
          transform="translate(9 6)"
          fill="#fff"
        />
      </g>
      <path
        d="M35.089 37.43l3.781-4.781-3.781-3.781"
        fill="none"
        stroke="#6966c7"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </svg>
  );
}

export function DesktopPreviousButton(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={73}
      height={73}
      viewBox="0 0 73 73"
      {...props}
    >
      <defs>
        <filter
          id="prefix__a"
          x={0}
          y={0}
          width={73}
          height={73}
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy={3} />
          <feGaussianBlur stdDeviation={3} result="b" />
          <feFlood floodOpacity={0.161} />
          <feComposite operator="in" in2="b" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g filter="url(#prefix__a)">
        <circle
          cx={27.5}
          cy={27.5}
          r={27.5}
          transform="translate(9 6)"
          fill="#fff"
        />
      </g>
      <path
        d="M37.571 30.23l-3.781 3.781 3.781 3.781"
        fill="none"
        stroke="#6966c7"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </svg>
  );
}

export function MobileLeftButton(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={100}
      height={100}
      style={{marginLeft : "-22px"}}
      viewBox="0 0 52.66 52.66"
      {...props}
    >
      <defs>
        <filter
          id="prefix__a"
          x={0}
          y={0}
          width={52.66}
          height={52.66}
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy={3} />
          <feGaussianBlur stdDeviation={3} result="b" />
          <feFlood floodOpacity={0.161} />
          <feComposite operator="in" in2="b" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g filter="url(#prefix__a)">
        <circle
          cx={17.33}
          cy={17.33}
          r={17.33}
          transform="rotate(180 21.83 20.33)"
          fill="#fff"
        />
      </g>
      <path
        d="M27.571 19.23l-3.781 3.781 3.781 3.781"
        fill="none"
        stroke="#6966c7"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </svg>
  );
}
