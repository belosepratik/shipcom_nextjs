import { createSlice } from "@reduxjs/toolkit";
export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: null,
    industryToggle : null,
  },
  reducers: {
    login: (state, action) => {
      state.user = action.payload;
    },
    logout: (state) => {
      state.user = null;
    },
    setToggleState: (state, action) => {
      state.industryToggle = action.payload;
    },
  },
});
 
export const { login, logout, setToggleState } = userSlice.actions;
export const selectUser = (state) => state.user.user;

export const selectIndustry = async (id) => (dispatch) => {
  setToggleState(id)
}

export default userSlice.reducer;