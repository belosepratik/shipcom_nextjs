import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../Redux/Slice/userSlice";
 
export default configureStore({
  reducer: {
    user: userReducer,
  },
});



