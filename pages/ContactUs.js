import Link from "next/link";
import React from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Layout from "../components/Layout";
import styles from "./css/Contact.module.css";
import GlobalCss from "./css/Global.module.css";

function ContactUs() {
  return (
    <>
      <div style={{overflow:"hidden"}}>
      <Header />
      <div
        style={{
          height: "500px",
          backgroundImage: "url('Images/Group_20650.png')",
        }}
      ></div>
      <div class="contanier">
        <div class="row">
          <div
            class="col-6"
            style={{
              backgroundColor: "#fff",
              height: "400px",
              margin: "0 auto",
            }}
          >
            <div class={styles.contact_details}>
              <img
                src="Images/mic.svg"
                alt=""
                style={{ width: "30px", height: "30px" }}
              />
              <p
                class={
                  GlobalCss.PoppinsSemiBold +
                  " " +
                  GlobalCss.H18 +
                  " " +
                  styles.contact_heading
                }
              >
                Contact sales
              </p>
              <p
                class={
                  GlobalCss.PoppinsRegular +
                  " " +
                  GlobalCss.H12 +
                  " " +
                  styles.contact_para
                }
              >
                We’re here to help you grow your business smarter. Call or email
                us to get in touch with our sales executives.
              </p>
              <div
                className={
                  "btn" +
                  " " +
                  GlobalCss.PoppinsRegular +
                  " " +
                  GlobalCss.H14 +
                  " " +
                  styles.btns
                }
              >
                {/* <p style={{color: "#fff",fontSize: "14px",margin:"0"}}>Request a demo</p> */}
                <span
                  class={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  style={{ color: "#fff", fontSize: "14px" }}
                >
                  Request a demo
                </span>
              </div>
              <p
                class={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                style={{ margin: "0" }}
              >
                <a
                  href=""
                  style={{
                    color: "#6966C7",
                    fontSize: "12px",
                    textDecoration: "none",
                  }}
                >
                  E: sales@shipcomwireless.com
                </a>
              </p>
              <p
                class={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                style={{ margin: "0" }}
              >
                <a
                  href=""
                  style={{
                    color: "#6966C7",
                    fontSize: "12px",
                    textDecoration: "none",
                  }}
                >
                  Add: 11200 Richmond Ave, Houston, TX 77082, United States
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <Footer />
      </div>
    </>
  );
}

export default ContactUs;
