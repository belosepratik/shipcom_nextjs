import Image from "next/image";
import React from "react";
import Header from "../components/Header";
import Layout from "../components/Layout";
import Colors from "../styles/Colors";
import Flickity from "react-flickity-component";
import GlobalCss from "./css/Global.module.css";

import styles from "./css/About.module.css";
import { BgAboutUs, RightArrowIcon } from "../assets/Icons/Icon";
import { Button } from "@material-ui/core";

const slider_data = [
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/work_builder.PNG",
  },
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/office_kitchen.PNG",
  },
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/carrier_work.PNG",
  },
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/work_builder.PNG",
  },
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/office_kitchen.PNG",
  },
  {
    title: "Building the modern analytics and BI team",
    desc:
      "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
    img: "/images/carrier_work.PNG",
  },
];

const AboutUs = () => {
  const flickityOptions = {
    groupCells: true,
    cellAlign: "left",
    pageDots: false,
    lazyLoad: true,
  };
  return (
    <>
      <div className={styles.about__bg_image}>
        <Header />
        <br />
        <br />
        <div>
          <div className="container">
            <h1
              className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
              style={{ color: "#383B62", opacity: 1 }}
            >
              Born in Texas, Shipcom has the vision <br /> to make the globe
              smarter through <br /> innovative, unconventional and
              <br /> seamless IoT based solutions
            </h1>
          </div>
          <br />
          <br />
          <br />
          <div style={{ marrginTop: "5rem" }} className="container">
            <div className="row" id={styles.mission_details}>
              <div className="col-sm">
                <p
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  style={{ color: Colors.orange, opacity: 1 }}
                >
                  Mission
                </p>
                <p
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  style={{ color: "#8A92A3", opacity: 1, fontSize: "14px" }}
                >
                  Save manual effort and automate processes to eradicate
                  <br />
                  inaccuracies from your business.
                </p>
              </div>
              <div className="col-sm">
                <p
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  style={{ color: Colors.orange, opacity: 1 }}
                >
                  Future goals
                </p>
                <p
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  style={{ color: "#8A92A3", opacity: 1 }}
                >
                  Shipcom aims to transform businesses all across the globe
                  <br /> whilst unleashing the true potential of the internet.
                </p>
              </div>
            </div>
            <div style={{ display: "flex", marginTop: "9rem" }}>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div
                  style={{
                    overflow: "hidden",
                    borderRadius: "7px",
                    marginRight: "1rem",
                  }}
                >
                  <Image src="/images/outing.PNG" width={412} height={320} />
                </div>
                <div
                  style={{
                    marginTop: "1rem",
                    overflow: "hidden",
                    borderRadius: "7px",
                  }}
                >
                  <Image
                    src="/images/group_meet.PNG"
                    width={412}
                    height={319}
                  />
                </div>
              </div>

              <div style={{ display: "flex", flexDirection: "column" }}>
                <div
                  style={{
                    overflow: "hidden",
                    borderRadius: "7px",
                    paddingRight: "1rem",
                  }}
                >
                  <Image
                    src="/images/office_meeting.PNG"
                    width={413}
                    height={401}
                  />
                </div>
                <div
                  style={{
                    marginTop: "1rem",
                    overflow: "hidden",
                    borderRadius: "7px",
                    display: "flex",
                  }}
                >
                  <Image
                    style={{ borderRadius: "7px" }}
                    src="/images/person.PNG"
                    width={204}
                    height={158}
                  />
                </div>
              </div>

              <div>
                <div
                  style={{
                    overflow: "hidden",
                    borderRadius: "7px",
                    marginRight: "1rem",
                  }}
                >
                  <Image src="/images/happy_guy.PNG" width={182} height={167} />
                </div>
              </div>
            </div>
            <div style={{ paddingTop: "4rem" }} className="row">
              <div
                style={{
                  width: "175px",
                  margin: "1rem",
                  textAlign: "center",
                  padding: "2rem",
                  boxShadow: "0px 10px 30px #d3d7de99",
                }}
                className="col"
              >
                <h1
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H30}
                  style={{ color: "#2C2E4B" }}
                >
                  56
                </h1>
                <p
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  style={{ color: "#8A92A3", fontSize: "14px" }}
                >
                  Companies worked with
                </p>
              </div>
              <div
                style={{
                  width: "175px",
                  margin: "1rem",
                  textAlign: "center",
                  padding: "2rem",
                  boxShadow: "0px 10px 30px #d3d7de99",
                }}
                className="col"
              >
                <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H30} style={{ color: "#2C2E4B"}}>20K</h1>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3"}}>
                  Sensors installed
                </p>
              </div>
              <div
                style={{
                  width: "175px",
                  margin: "1rem",
                  textAlign: "center",
                  padding: "2rem",
                  boxShadow: "0px 10px 30px #d3d7de99",
                }}
                className="col"
              >
                <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H30} style={{ color: "#2C2E4B"}}>2.3M</h1>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3"}}>
                  End users impacted
                </p>
              </div>
              <div
                style={{
                  width: "175px",
                  margin: "1rem",
                  textAlign: "center",
                  padding: "2rem",
                  boxShadow: "0px 10px 30px #d3d7de99",
                }}
                className="col"
              >
                <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H30} style={{ color: "#2C2E4B"}}>225</h1>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3"}}>
                  Number of employees
                </p>
              </div>
              <div
                style={{
                  width: "175px",
                  margin: "1rem",
                  textAlign: "center",
                  padding: "2rem",
                  boxShadow: "0px 10px 30px #d3d7de99",
                }}
                className="col"
              >
                <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H30} style={{ color: "#2C2E4B"}}>3</h1>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3"}}>
                  No of offices
                </p>
              </div>
            </div>
          </div>
          <div className={styles.about_bg_gradient}>
            <div
              style={{ textAlign: "center", padding: "5rem" }}
              className="container"
            >
              <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34} style={{ color: "#fff"}}>
                What we value at Shipcom
              </h1>
              <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#FFFFFF66"}}>
                Like every human being, we might be astray at times. These
                <br /> values lead us directly to our vision
              </p>
            </div>

            <div style={{ paddingBottom: "4rem" }} className="container">
              <div style={{ textAlign: "center" }} className="row">
                <div className="col">
                  <h3 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18} style={{ color: "#fff"}}>
                    Commitment to customers
                  </h3>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12} style={{ color: Colors.lightGray, fontSize: "12px" }}>
                    We’re honest and upfront with our deliverables with a <br />
                    mindset to solve customer problems at the earliest.
                  </p>
                </div>
                <div className="col">
                  <h3 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18} style={{ color: "#fff"}}>
                    Continuous learning
                  </h3>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12} style={{ color: Colors.lightGray}}>
                    At Shipcom, we believe in the process of learning new
                    <br /> skills and knowledge on an on-going basis.
                  </p>
                </div>
              </div>
              <hr
                style={{
                  background: Colors.lightGray,
                  marginTop: "3rem",
                  marginBottom: "3rem",
                }}
              />
              <div style={{ textAlign: "center" }} className="row">
                <div className="col">
                  <h3 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18} style={{ color: "#fff"}}>
                    Constant improvement
                  </h3>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12} style={{ color: Colors.lightGray}}>
                    We believe in identifying opportunities for work <br />
                    process enhancements.
                  </p>
                </div>
                <div className="col">
                  <h3 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18} style={{ color: "#fff"}}>
                    Collective work
                  </h3>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12} style={{ color: Colors.lightGray}}>
                    Our team understands the importance of working
                    <br /> smoothly in a cross-business unit set-up.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div style={{ textAlign: "center", paddingTop: "6rem" }}>
            <h2 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34} style={{ color: "#383B62",opacity: 1 }}>
              The team
            </h2>
            <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3", opacity: 1 }}>
              Meet our leadership team, building and nurturing geniuses
              <br />
              at Shipcom
            </p>
          </div>
          <div className="container">
            <div className="row">
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm team-images"
              >
                <Image src="/images/ceo.PNG" width={400} height={300} />
              </div>
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm"
              >
                <Image src="/images/cto.PNG" width={400} height={300} />
              </div>
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm"
              >
                <Image src="/images/cfo.PNG" width={400} height={300} />
              </div>
            </div>
            <div className="row">
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm"
              >
                <Image src="/images/designer.PNG" width={400} height={300} />
              </div>
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm"
              >
                <Image src="/images/artist.PNG" width={400} height={300} />
              </div>
              <div
                style={{
                  height: "320px",
                  margin: "1rem",
                  borderRadius: "5px",
                  overflow: "hidden",
                }}
                className="col-sm"
              >
                <Image src="/images/developer.PNG" width={400} height={300} />
              </div>
            </div>

            <div style={{ display: "flex", justifyContent: "center" }}>
              <div className={styles.wrapper}>
                <div className={styles.btn}>
                  <button
                    type="button"
                    style={{
                      background: "none",
                      color: Colors.blue,
                      width: "130px",
                      height: "40px",
                      border: `1px solid ${Colors.blue}`,
                      fontSize: "14px",
                      borderRadius: "4px",
                      transition: "0.6s",
                      overflow: "hidden",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14 + " " + styles.button}
                  >
                    See more
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div style={{ paddingTop: "5rem" }} className="container">
            <p className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14} style={{ color: Colors.gray}}>
              AWARDS WE'VE WON
            </p>
            <div className="row">
              <div className="col">
                <Image src="/images/google.PNG" height={50} width={130} />
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H16}
                  style={{
                    color: Colors.lightDenim,
                  }}
                >
                  Award name
                </p>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: Colors.gray}}>
                  One liner description <br /> on what the award is about
                </p>
              </div>
              <div className="col">
                <Image src="/images/red.PNG" height={50} width={100} />
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H16}
                  style={{
                    color: Colors.lightDenim,
                  }}
                >
                  Award name
                </p>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: Colors.gray}}>
                  One liner description <br /> on what the award is about
                </p>
              </div>
              <div className="col">
                <Image src="/images/coke.PNG" height={50} width={100} />
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H16}
                  style={{
                    color: Colors.lightDenim,
                  }}
                >
                  Award name
                </p>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: Colors.gray}}>
                  One liner description <br /> on what the award is about
                </p>
              </div>
              <div className="col">
                <Image src="/images/coke.PNG" height={50} width={100} />
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H16}
                  style={{
                    color: Colors.lightDenim,
                  }}
                >
                  Award name
                </p>
                <p  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: Colors.gray}}>
                  One liner description <br /> on what the award is about
                </p>
              </div>
            </div>
          </div>
          <div
            style={{ backgroundColor: Colors.ghostWhite, paddingTop: "4rem" }}
          >
            <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
              style={{
                color: Colors.lightDenim,
                textAlign: "center",
              }}
            >
              The media seems to like us!
            </p>
            <p className={GlobalCss.PoppinsRegular+ " " + GlobalCss.H14}
              style={{
                textAlign: "center",
                color: Colors.gray,
              }}
            >
              We’ve been in the news lately for this - here are snippets of what
              we
              <br /> have been up to!
            </p>
            <br />
            <div className="container">
              <Flickity
                id="carousel"
                className={styles.carousel__main} // default ''
                elementType={"div"} // default 'div'
                options={flickityOptions} // takes flickity options {}
                disableImagesLoaded={false} // default false
                reloadOnUpdate // default false
                static // default false
              >
                {slider_data.map((item, index) => {
                  return (
                    <div className={styles.carousel__cell}>
                      <div style={{ height: "300px" }}>
                        <Image src={item.img} height={300} width={364}></Image>
                      </div>
                      <div style={{ padding: "2rem" }}>
                        <p className={GlobalCss.PoppinsMedium+ " " + GlobalCss.H18}
                          style={{ color: Colors.lightDenim}}
                        >
                          {item.title}
                        </p>
                        <p className={GlobalCss.PoppinsRegular+ " " + GlobalCss.H14} style={{ color: Colors.gray}}>
                          {item.desc}
                        </p>
                        <div className={styles.wrapper}>
                          <div className={styles.btn}>
                            <button
                              type="button"
                              style={{
                                background: "none",
                                color: Colors.blue,
                                width: "140px",
                                height: "40px",
                                border: `1px solid ${Colors.blue}`,
                                fontSize: "14px",
                                borderRadius: "4px",
                                transition: "0.6s",
                                overflow: "hidden",
                              }}
                              className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14 + " " + styles.button}
                            >
                              Read more &nbsp; <RightArrowIcon />
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </Flickity>
            </div>
          </div>
          <br />
          <br />
          <div className="container">
            <div className={styles.about__work_bg}>
              <div className="row">
                <div
                  style={{ padding: "5rem 0rem 5rem 5rem" }}
                  className="col-sm"
                >
                  <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34} style={{color: Colors.white }}>
                    Are you ready to be part of our team?
                  </p>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: Colors.lightGray }}>
                    We’re constantly growing and if you think you fit in,
                    <br /> we’d love to explore the opportunity.
                  </p>
                  <div className={styles.wrapper}>
                    <div className={styles.btn}>
                      <button
                        type="button"
                        style={{
                          background: "none",
                          color: Colors.white,
                          width: "140px",
                          height: "40px",
                          border: `1px solid ${Colors.white}`,
                          fontSize: "16px",
                          borderRadius: "4px",
                          transition: "0.6s",
                          overflow: "hidden",
                        }}
                        className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14 + " " + styles.button}
                      >
                        Join the team
                      </button>
                    </div>
                  </div>
                </div>
                <div style={{ padding: "3rem" }} className="col-sm">
                  <Image src="/images/team_clap.PNG" width={630} height={400} />
                </div>
              </div>
            </div>
          </div>
          <br /> <br />
          <div
            style={{
              // background: "blue",
              textAlign: "center",
              backgroundImage: `url('/images/bg_101.svg')`,
              backgroundRepeat: "repeat",
              backgroundSize: "600px 900px",
              // backgroundSize: "cover",
            }}
            // className="container"
          >
            <div className="container" style={{ padding: "8rem 0rem" }}>
              <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34} style={{color:"#383B62"}}> Let’s work together!</p>
              <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#383B62" }}>
                Give us a problem and we’ll find a solution for you
              </p>
              <Button className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                style={{ textTransform: "none" }}
                color="primary"
                variant="contained"
              >
                Request a demo
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutUs;