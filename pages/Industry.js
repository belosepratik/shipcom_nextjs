import React from "react";
import { SmallRightIcon } from "../assets/Icons/Icon";
import Colors from "../styles/Colors";
import GlobalCss from "./css/Global.module.css";

const Industry = () => {
  return (
    <div style={{ padding: "6rem" }} className="container-fluid">
      <div className="row">
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Agriculture   </span>
          <SmallRightIcon />
          <br />
          <p
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
          >
            Manage your entire farming cycle remotely.
          </p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Education   </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Safely open campuses by monitoring occupancy.
          </p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Manufacturing   
          </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Innovatively collaborate your manufacturing using IoT devices.
          </p>
        </div>
      </div>
      <div className="row">
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Aviation   </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Track baggage, enable cabin climate control and more.
          </p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Food & Beverage   </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Get visibility on food quality, production and transportation.
          </p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Pharmaceutical   </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Create optimal conditions for your drug manufacturing environment.
          </p>
        </div>
      </div>
      <div className="row">
        <div
          style={{
            margin: "2rem",
            paddingTop: "1rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Commercial Real Estate   </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Automate commercial spaces and make them cost-effective.
          </p>
        </div>
        <div
          style={{
            margin: "2rem",
            backgroundColor: Colors.blue,
            borderRadius: "10px",
            paddingTop: "1rem",
          }}
          className="col"
        >
          <span style={{ color: Colors.white }}>
            <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
              Healthcare       </span>
            <SmallRightIcon />
          </span>
          <br />
          <p
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
            style={{ color: Colors.lightGray }}
          >
            Automated solutions for modern healthcare institutions.
          </p>
        </div>
        <div
          style={{
            margin: "2rem",
            paddingTop: "1rem",
          }}
          className="col"
        >
          <span className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}>
            Retail       </span>
          <SmallRightIcon />
          <br />
          <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}>
            Optimize the in-store experience of your shoppers.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Industry;
