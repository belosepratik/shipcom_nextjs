import { Button } from "@material-ui/core";
import Image from "next/image";
import React from "react";
import Header from "../components/Header";
import Colors from "../styles/Colors";
import Footer from "../components/Footer";
import styles from "./css/FoodAndBeverage.module.css";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import IntroSection from "../components/Industry/IntroSection";
import DescribeSection from "../components/Industry/DescribeSection";
import OfferingSection from "../components/Industry/OfferingSection";
import HowSection from "../components/Industry/HowSection";
import Testimonial from "../components/Industry/Testimonial";
import GlobalCss from "./css/Global.module.css";

const FoodAndBeverage = () => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");
  const headerContent = {
    title: (
      <>
        <span className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
          Next generation{" "}
        </span>
        <span
          className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
          style={{ color: Colors.orange }}
        >
          food and <br /> beverage
        </span>
        <span className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
          {" "}
          management
        </span>
      </>
    ),
    subTitle: (
      <>
        <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
          Shipcom’s automation of food and beverage
        </span>
        {isTabletOrMobileDevice && <br />}{" "}
        <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
          supply chains and
        </span>
        {!isTabletOrMobileDevice && <br />}{" "}
        <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
          inventory optimizes resources and prevents product loss.
        </span>
      </>
    ),

    image: "/images/chef.PNG",
  };

  const describeContent = {
    describe1: "Streamline logistics",
    subDescribe1:
      "GPS sensors detect the real-time location of all tracked vehicles and provide recommended routes to avoid traffic delays or road closures, improving overall fleet management.",
    describe2: "Enrich dine-in experiences",
    subDescribe2:
      "Safely offer dine-in experiences by seamlessly managing table bookings and keeping track of real-time occupancy limits to ensure safety and compliance.",
    describe3: "Ensure food safety",
    subDescribe3:
      "Closely track the temperature and environmental conditions of perishable food items in transit and on-site. Ensure sensitive items are stored at proper temperatures to prevent spoilage and reduce risk of product loss.",
  };

  const offerContent = {
    title: "Our offerings",
    subTitle: (
      <>
        We offer the best-in-class IoT solutions to
        {isTabletOrMobileDevice && <br />} automate your business
        {!isTabletOrMobileDevice && <br />} backed by powerful and value-driven
        insights
      </>
    ),
  };
  return (
    <>
      <Header />

      <IntroSection content={headerContent} />

      <DescribeSection content={describeContent} />

      <OfferingSection content={offerContent} />

      <HowSection />

      <Testimonial />
    </>
  );
};

export default FoodAndBeverage;
