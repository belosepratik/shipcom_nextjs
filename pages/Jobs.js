import React from "react";
import Layout from "../components/Layout";
import SearchIcon from "@material-ui/icons/Search";
import { ArrowDown } from "../assets/Icons/Icon";

import styles from "./css/Jobs.module.css";
import GlobalCss from "./css/Global.module.css";

const table_content = [
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Internship",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
  {
    name: "Account associate",
    role: "Sales",
    city: "Houston",
    duration: "Full-time",
  },
];

const Jobs = () => {
  return (
    <Layout>
      <div>
        <div style={{ padding: "4rem" }} className="container">
          <h1  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34} style={{ textAlign: "center" }} > Current openings</h1>
          <p  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ textAlign: "center" }}>100 openings</p>
        </div>
      </div>
      <div className={styles.container}>
        <div className="row">
          <div className="col-sm">
            <div className={styles.jobs__searchbar}>
              <input placeholder="Search" className={styles.jobsInput} type="text" />
              <div style={{ paddingRight: "8px" }}>
                <SearchIcon />
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className={styles.jobs__searchbar}>
              <input placeholder="Department" className={styles.jobsInput} type="text" />
              <div style={{ paddingRight: "8px" }}>
                <ArrowDown />
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className={styles.jobs__searchbar}>
              <input placeholder="Location" className={styles.jobsInput} type="text" />
              <div style={{ paddingRight: "8px" }}>
                <ArrowDown />
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className={styles.jobs__searchbar}>
              <input placeholder="Type" className={styles.jobsInput} type="text" />
              <div style={{ paddingRight: "8px" }}>
                <ArrowDown />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ paddingTop: "4rem" }}>
        <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H16} style={{ textAlign: "center" }}>
          100 roles across all locations and departments
        </p>
        <br />
        <div style={{ padding: "3rem" }}>
          <table class="table table-striped">
            <tbody>
              {table_content.map((item, index) => {
                return (
                  <tr>
                    <th className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H16} scope="row" >{item.name}</th>
                    <td className={GlobalCss.PoppinsMedium + " " + GlobalCss.H16}>{item.role}</td>
                    <td className={GlobalCss.PoppinsMedium + " " + GlobalCss.H16}>{item.city}</td>
                    <td className={GlobalCss.PoppinsMedium + " " + GlobalCss.H16}>{item.duration}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default Jobs;
