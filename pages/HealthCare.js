import { Button } from "@material-ui/core";
import Image from "next/image";
import React from "react";
import Header from "../components/Header";
import Colors from "../styles/Colors";
import Footer from "../components/Footer";
import styles from "./css/FoodAndBeverage.module.css";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import IntroSection from "../components/Industry/IntroSection";
import DescribeSection from "../components/Industry/DescribeSection";
import OfferingSection from "../components/Industry/OfferingSection";
import HowSection from "../components/Industry/HowSection";
import Testimonial from "../components/Industry/Testimonial";
import GlobalCss from "./css/Global.module.css";

const HealthCare = () => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");
  const headerContent = {
    title: (
      <>
        Transforming <span style={{ color: Colors.orange }}>healthcare </span>
        operations & quality of care
      </>
    ),
    subTitle: (
      <>
        Shipcom improves healthcare operational workflow & the quality of
        patient care via the industry’s most advanced & flexible IoT platform
      </>
    ),

    image: "/images/doctor.PNG",
  };

  const describeContent = {
    describe1: "Patient locating & monitoring",
    subDescribe1:
      "Wearable technology, smartphones as well as specialty sensors can be worn by patients, so care providers can access vital health and location data to offer more responsive care.",
    describe2: "Smarter healthcare",
    subDescribe2:
      "Tags and sensors can track critical, and life-saving equipment, and locate staff members, optimizing workflow and room management.",
    describe3: "More complete health records for care providers",
    subDescribe3:
      "Healthcare professionals are able to access automated medical records gathered through the association of IoT data from patients, medical equipment and staff.",
  };

  const offerContent = {
    title: "Our offerings",
    subTitle: (
      <>
        Healthcare professionals are able to access automated medical records
        gathered through the association of IoT data from patients, medical
        equipment and staff.
      </>
    ),
  };
  return (
    <>
      <Header />

      <IntroSection content={headerContent} />

      <DescribeSection content={describeContent} />

      <OfferingSection content={offerContent} />

      <HowSection />

      <Testimonial />
    </>
  );
};

export default HealthCare;
