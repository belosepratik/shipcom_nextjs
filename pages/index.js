import React from "react";
import Header from "../components/Header";
import styles from "./css/Home.module.css";
import Footer from "../components/Footer";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Colors from "../styles/Colors";
import { RightArrowIcon } from "../assets/Icons/Icon";
import { Button, Typography, withStyles } from "@material-ui/core";
import ScrollableTabsButtonAuto from "../components/Tabs";
import OfferingSection from "../components/Industry/OfferingSection";
import Testimonial from "../components/Industry/Testimonial";
import GlobalCss from "./css/Global.module.css";
import SwipeableTemporaryDrawer from "../components/SwipeableTemporaryDrawer";
import Marquee from "react-fast-marquee";
// import {OverlayTrigger, Tooltip} from 'react-bootstrap';
// import Button from '@material-ui/core/Button';
import Tooltip from "@material-ui/core/Tooltip";

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: "#fff",
    color: "rgba(0, 0, 0, 0.87)",
    maxWidth: 400,
    maxHeight:200,
    fontSize: theme.typography.pxToRem(12),
    // border: "1px solid #dadde9",
    borderRadius: "5px",
    boxShadow: "0px 0px 17px #6966C727"
  },
}))(Tooltip);

const Home = () => {
  const matches = useMediaQuery("(max-width:480px)");

  const [toggleState, setToggleState] = React.useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };
  const longText = `
  Agriculture >
  Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.
  `;
  const offerContent = {
    title: "Our offerings",
    subTitle: (
      <>
        We offer the best-in-class IoT solutions to
        {matches && <br />} automate your business
        {!matches && <br />} backed by powerful and value-driven insights
      </>
    ),
  };

  return (
    <>
      <div
        className="container-fluid"
        style={{
          backgroundImage: "url('images/MaskGroup101.png')",
          backgroundColor: "#D3D7DE21",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "contain",
          // height: `${matches ? "98vh" : "90vh"}`,
          height: "666px",
        }}
      >
        {/* <Header /> */}
        {matches ? (
          <>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <img src="images/MaskGroup162@3x.png" style={{ width: "40%" }} />
              <SwipeableTemporaryDrawer />
            </div>
          </>
        ) : (
          <Header />
        )}
        <div className="container">
          <div
            className="row"
            style={{ marginTop: `${matches ? "5rem" : "0rem"}` }}
          >
            <div
              style={{ paddingBottom: `${matches ? "3rem" : "0rem"}` }}
              className="col-sm-12 col-md-6"
            >
              <div style={{ padding: `${matches ? "0rem" : "5rem"}` }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: `${matches ? "center" : "flex-start"}`,
                  }}
                >
                  <h1
                    onClick={() => toggleTab(1)}
                    href="#"
                    style={{
                      color: `${toggleState === 1 ? "#eda343" : "#2B2D49"} `,
                      cursor: "pointer",
                    }}
                    className={
                      GlobalCss.PoppinsSemiBold +
                      " " +
                      `${matches ? GlobalCss.H24 : GlobalCss.H34}`
                    }
                  >
                    Control.
                  </h1>
                  <h1
                    onClick={() => toggleTab(2)}
                    href="#"
                    style={{
                      color: `${toggleState === 2 ? "#eda343" : "#2B2D49"} `,
                      cursor: "pointer",
                    }}
                    className={
                      GlobalCss.PoppinsSemiBold +
                      " " +
                      `${matches ? GlobalCss.H24 : GlobalCss.H34}`
                    }
                  >
                    Track.
                  </h1>
                  <h1
                    onClick={() => toggleTab(3)}
                    href="#"
                    style={{
                      color: `${toggleState === 3 ? "#eda343" : "#2B2D49"}`,
                      cursor: "pointer",
                    }}
                    className={
                      GlobalCss.PoppinsSemiBold +
                      " " +
                      `${matches ? GlobalCss.H24 : GlobalCss.H34}`
                    }
                  >
                    Monitor.
                  </h1>
                </div>
                <p
                  style={{
                    color: "#8A92A3",
                    fontSize: `${matches ? "13px" : "14px"}`,
                    textAlign: `${matches ? "center" : "left"}`,
                  }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                >
                  Say no manual work. Automate and optomize your business with
                  out best-in-class IoT offerings that operateon a single
                  cloud-based console.
                </p>
                <div className={styles.hashTagHome}>
                  <div
                    style={{
                      paddingLeft: "10px",
                      marginRight: "5px",
                      borderRadius: "10px",
                      paddingRight: "10px",
                      color: "#E0922D",
                      backgroundColor: "#FFC47734",
                    }}
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                  >
                    #Manufacturing
                  </div>
                  <div
                    style={{
                      paddingLeft: "10px",
                      marginRight: "5px",
                      borderRadius: "10px",
                      paddingRight: "10px",
                      color: "#E0922D",
                      backgroundColor: "#FFC47734",
                    }}
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                  >
                    #Healthcare
                  </div>
                  <div
                    style={{
                      paddingLeft: "10px",
                      marginRight: "5px",
                      borderRadius: "10px",
                      paddingRight: "10px",
                      color: "#E0922D",
                      backgroundColor: "#FFC47734",
                    }}
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                  >
                    #foodSupply
                  </div>
                  <div
                    style={{
                      paddingLeft: "10px",
                      marginRight: "5px",
                      borderRadius: "10px",
                      paddingRight: "10px",
                      color: "#E0922D",
                      backgroundColor: "#FFC47734",
                    }}
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                  >
                    #Agriculture
                  </div>
                </div>
                <br />
                <div
                  style={{
                    display: "flex",
                    justifyContent: `${matches ? "center" : "flex-start"}`,
                  }}
                >
                  <Button
                    style={{
                      textTransform: "none",
                      padding: "10px 15px",
                      background: "#6966C7",
                    }}
                    variant="contained"
                  >
                    <span
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                      style={{ color: "#fff" }}
                    >
                      Request a demo
                    </span>
                  </Button>
                </div>
              </div>
            </div>

            <div id={styles.right__box} className="col-sm-12 col-md-6">
              {toggleState === 1 && (
                // <img
                //   style={{
                //     height: `${matches ? "15rem" : "20rem"}`,
                //     maxWidth: "100%",
                //   }}
                //   src="images/Control.png"
                // />
                <video
                  autoPlay={true}
                  loop={true}
                  style={{
                    height: `${matches ? "15rem" : "20rem"}`,
                    maxWidth: "100%",
                  }}
                >
                  <source src="videos/Control.mp4" />
                </video>
              )}
              {toggleState === 2 && (
                <video
                  autoPlay={true}
                  loop={true}
                  style={{
                    height: `${matches ? "15rem" : "20rem"}`,
                    maxWidth: "100%",
                  }}
                >
                  <source src="videos/Track.mp4" />
                </video>
              )}
              {toggleState === 3 && (
                <video
                  autoPlay={true}
                  loop={true}
                  style={{
                    height: `${matches ? "15rem" : "20rem"}`,
                    maxWidth: "100%",
                  }}
                >
                  <source src="videos/Monitor.mp4" />
                </video>
              )}
            </div>
          </div>
        </div>

        {toggleState === 1 && (
          <>
            <div
              style={{
                display: "flex",
                paddingTop: `${matches ? "2rem" : "0rem"}`,
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  display: "flex",
                  boxShadow: "0px 10px 26px #D3D7DE5A",
                  borderRadius: "5px",
                  overflow: "hidden",
                  maxWidth: "450px",
                  flexDirection: `${matches ? "column" : "row"} `,
                  // padding: `${matches ? "1.5rem" : "0rem"}`,
                  margin: "1.4rem",
                  backgroundColor: Colors.white,
                }}
              >
                <div>
                  <img
                    src="images/331da53759dd01d644dd990360854eedu.png"
                    style={{
                      width: `${matches ? "100%" : "140px"} `,
                      height: `${matches ? "18rem" : "10rem"}`,
                    }}
                  />
                </div>
                <div
                  style={{
                    padding: "10px",
                    paddingLeft: "1.5rem",
                  }}
                >
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{ color: "#6966C7" }}
                    >
                      NEWS
                    </div>
                    <span
                      style={{
                        padding: "3px",
                        paddingLeft: "5px",
                        paddingRight: "5px",
                        borderRadius: "12px",
                        backgroundColor: "#FFC477",
                        color: Colors.white,
                        fontWeight: "600",
                      }}
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      New
                    </span>
                  </div>
                  <p
                    style={{ color: "#383B62" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    Winners : AIM case study for developing compelling global
                    IoT solutions
                  </p>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <p
                      style={{ color: "#8A92A3B3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                    >
                      Dec 12, 2020
                    </p>
                    <RightArrowIcon />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}

        {toggleState === 2 && (
          <>
            <div
              style={{
                display: "flex",
                paddingTop: `${matches ? "2rem" : "0rem"}`,
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  display: "flex",
                  boxShadow: "0px 10px 26px #D3D7DE5A",
                  borderRadius: "5px",
                  overflow: "hidden",
                  maxWidth: "450px",
                  flexDirection: `${matches ? "column" : "row"} `,
                  // padding: `${matches ? "1.5rem" : "0rem"}`,
                  margin: "1.4rem",
                  backgroundColor: Colors.white,
                }}
              >
                <div>
                  <img
                    src="images/331da53759dd01d644dd990360854eedu.png"
                    style={{
                      width: `${matches ? "100%" : "140px"} `,
                      height: `${matches ? "18rem" : "10rem"}`,
                    }}
                  />
                </div>
                <div
                  style={{
                    padding: "10px",
                    paddingLeft: "1.5rem",
                  }}
                >
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{ color: "#6966C7" }}
                    >
                      WHITE PAPER
                    </div>
                    <span
                      style={{
                        padding: "3px",
                        paddingLeft: "5px",
                        paddingRight: "5px",
                        borderRadius: "12px",
                        backgroundColor: "#FFC477",
                        color: Colors.white,
                        fontWeight: "600",
                      }}
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      New
                    </span>
                  </div>
                  <p
                    style={{ color: "#383B62" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    Helping Harris Health Systems reimagine cold-chain supply
                  </p>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <p
                      style={{ color: "#8A92A3B3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                    >
                      Dec 12, 2020
                    </p>
                    <RightArrowIcon />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
        {toggleState === 3 && (
          <>
            <div
              style={{
                display: "flex",
                paddingTop: `${matches ? "2rem" : "0rem"}`,
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  display: "flex",
                  boxShadow: "0px 10px 26px #D3D7DE5A",
                  borderRadius: "5px",
                  overflow: "hidden",
                  maxWidth: "450px",
                  flexDirection: `${matches ? "column" : "row"} `,
                  // padding: `${matches ? "1.5rem" : "0rem"}`,
                  margin: "1.4rem",
                  backgroundColor: Colors.white,
                }}
              >
                <div>
                  <img
                    src="images/331da53759dd01d644dd990360854eedu.png"
                    style={{
                      width: `${matches ? "100%" : "140px"} `,
                      height: `${matches ? "18rem" : "10rem"}`,
                    }}
                  />
                </div>
                <div
                  style={{
                    padding: "10px",
                    paddingLeft: "1.5rem",
                  }}
                >
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{ color: "#6966C7" }}
                    >
                      BLOG
                    </div>
                    <span
                      style={{
                        padding: "3px",
                        paddingLeft: "5px",
                        paddingRight: "5px",
                        borderRadius: "12px",
                        backgroundColor: "#FFC477",
                        color: Colors.white,
                        fontWeight: "600",
                      }}
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      New
                    </span>
                  </div>
                  <p
                    style={{ color: "#383B62" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    IoT Sensing Technologies can accelerate the safe
                    Distribution of COVID-19 Vaccines
                  </p>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <p
                      style={{ color: "#8A92A3B3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                    >
                      Dec 12, 2020
                    </p>
                    <RightArrowIcon />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            paddingTop: "1rem",
          }}
        >
          <div
            style={{
              width: "6rem",
              // height: "2rem",
              display: "flex",
              backgroundColor: "#F3F5F8",
              borderRadius: "13px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {toggleState === 1 ? (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#6966C7",
                  borderRadius: "7px",
                  cursor: "pointer",
                }}
              />
            ) : (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#D3D7DE",
                  borderRadius: "50%",
                  cursor: "pointer",
                }}
                onClick={() => toggleTab(1)}
              />
            )}
            &nbsp; &nbsp;
            {toggleState === 2 ? (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#6966C7",
                  borderRadius: "7px",
                  cursor: "pointer",
                }}
              />
            ) : (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#D3D7DE",
                  borderRadius: "50%",
                  cursor: "pointer",
                }}
                onClick={() => toggleTab(2)}
              />
            )}
            &nbsp; &nbsp;
            {toggleState === 3 ? (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#6966C7",
                  borderRadius: "7px",
                  cursor: "pointer",
                }}
              />
            ) : (
              <div
                style={{
                  width: "8px",
                  height: "8px",
                  backgroundColor: "#D3D7DE",
                  borderRadius: "50%",
                  cursor: "pointer",
                }}
                onClick={() => toggleTab(3)}
              />
            )}
          </div>
        </div>
      </div>

      <br />
      <br />
      <br />
      <br />

      <div
        style={{ paddingTop: `${matches ? "33rem" : "10rem"}` }}
        className="container"
      >
        <div className="row">
          <div
            style={{
              margin: "0 auto",
              width: `${matches ? "95%" : "45%"}`,
              padding: "2%",
            }}
          >
            <h1
              style={{ color: "#383b62", textAlign: "center", padding: "0 4%" }}
              className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
            >
              Why does the world need IoT?
            </h1>
            <br />
            <p
              className={GlobalCss.PoppinsRegular}
              style={{
                fontSize: `${matches ? "12px" : "14px"}`,
                textAlign: "center",
                color: "#8A92A3B3",
              }}
              className={GlobalCss.PoppinsMedium}
            >
              Internet of things (IoT) is connecting bllions of physical devices
              through the internet to convert Earth into a smarter planet
            </p>
          </div>
        </div>
      </div>

      <br />
      <br />
      <br />

      {matches ? (
        <ScrollableTabsButtonAuto />
      ) : (
        <div className="container">
          <div className="row" id={styles.iot__product}>
            <div className="col-md-3">
              <img src="images/Layer 2-6.png" style={{ height: "102px" }} />
              <h4 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                Sensors and tags
              </h4>
              <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                We offer smart sensors, tags, and instructions from all the
                leading manufactures, allowing us to deploy the optimal mix of
                sensory hardware for every Iot Application.
              </span>
            </div>
            <div className="col-md-3">
              <img src="images/Layer2.png" style={{ height: "102px" }} />
              <h4 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                Data Collection
              </h4>
              <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                Each sensor is programmed to collect specific data through
                gateways which can include parameters like location,
                temperature, air quality, humidity, recent activity & more.
              </span>
            </div>
            <div className="col-md-3">
              <img src="images/Layer 2-4.png" style={{ height: "102px" }} />
              <h4 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                Cloud-Based Connectivity
              </h4>
              <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                Gateways are connected via the internet through either WiFi or
                Cellular LTE (3G/4G/5G), so all collected sensor data can be
                transferred to the cloud in a secure, reliable network.
              </span>
            </div>
            <div className="col-md-3">
              <img src="images/Layer 2-5.png" style={{ height: "102px" }} />
              <h4 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                Stunning insights
              </h4>
              <span className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                The data is structured to create invaluable insights which can
                be accessed easily through mobile or desktop via{" "}
                <span
                  className={GlobalCss.PoppinsSemiBold}
                  style={{ color: "#6966C7" }}
                >
                  Catamaran NextGen™
                </span>
                , our proprietary console.
              </span>
            </div>
          </div>
        </div>
      )}

      <br />
      <br />

      <div
        style={{
          backgroundColor: "#FAFAFB",
          paddingTop: "5rem",
          paddingBottom: "5rem",
        }}
        className="container-fluid"
        // id={styles.shipcom__product}
      >
        <div
          style={{
            width: `${matches ? "100%" : "40%"}`,
            margin: "0 auto 60px auto",
          }}
        >
          <h1
            style={{ color: "#383b62", textAlign: "center", marginBottom: 20 }}
            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
          >
            Why Shipcom?
          </h1>
          <p
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
            style={{ color: "#969ead", textAlign: "center", margin: 0 }}
          >
            We make your everyday lide smarter by providing real-time data from
            a multitude of connected devices
          </p>
        </div>
        <div
          style={{ width: `${matches ? "95%" : "60%"}` }}
          className="container"
        >
          <div class="row">
            <div style={{ padding: "1rem" }} className="col-sm">
              <div
                style={{
                  padding: "35px 30px",
                  minHeight: "220px",
                  boxShadow: "0px 10px 30px #D3D7DE99",
                  backgroundColor: "#fff",
                }}
              >
                <img src="images/id-card.png" />
                <h5
                  className={
                    GlobalCss.PoppinsSemiBold +
                    " " +
                    GlobalCss.H18 +
                    " " +
                    "card-title"
                  }
                  style={{ paddingTop: "10px" }}
                >
                  Catamaran NextGen™
                </h5>
                <p
                  className={
                    GlobalCss.PoppinsRegular +
                    " " +
                    GlobalCss.H12 +
                    " " +
                    "card-text"
                  }
                  style={{ paddingTop: "10px" }}
                  style={{ color: "#8A92A3" }}
                >
                  Accessible through mobile and desktop, our proprietary
                  application provides you with the dedicated insights and
                  reports.
                </p>
              </div>
            </div>
            <div style={{ padding: "1rem" }} className="col-sm">
              <div
                style={{
                  padding: "35px 30px",
                  minHeight: "220px",
                  boxShadow: "0px 10px 30px #D3D7DE99",
                  backgroundColor: "#fff",
                }}
              >
                <img src="images/totalPackage.png" />
                <h5
                  className={
                    GlobalCss.PoppinsSemiBold +
                    " " +
                    GlobalCss.H18 +
                    " " +
                    "card-title"
                  }
                  style={{ paddingTop: "10px" }}
                >
                  Total package solution
                </h5>
                <p
                  className={
                    GlobalCss.PoppinsRegular +
                    " " +
                    GlobalCss.H12 +
                    " " +
                    "card-text"
                  }
                  style={{ color: "#8A92A3" }}
                >
                  Eliminate third-party interference and save time with us. Our
                  applications are designed to be compatible across most cost
                  common systems.
                </p>
              </div>
            </div>
            <div className="w-100"></div>
            <div style={{ padding: "1rem" }} className="col-sm">
              <div
                style={{
                  padding: "35px 30px",
                  minHeight: "220px",
                  boxShadow: "0px 10px 30px #D3D7DE99",
                  backgroundColor: "#fff",
                }}
              >
                <img src="images/easyInatall.png" />
                <h5
                  className={
                    GlobalCss.PoppinsSemiBold +
                    " " +
                    GlobalCss.H18 +
                    " " +
                    "card-title"
                  }
                  style={{ paddingTop: "10px" }}
                >
                  Easy installations & integrations
                </h5>
                <p
                  className={
                    GlobalCss.PoppinsRegular +
                    " " +
                    GlobalCss.H12 +
                    " " +
                    "card-text"
                  }
                  style={{ color: "#8A92A3" }}
                >
                  Our smart sensors install within minutes without disrupting
                  your ongoing operations. We've designed them to fit into your
                  existing software to save time.
                </p>
              </div>
            </div>
            <div style={{ padding: "1rem" }} className="col-sm">
              <div
                style={{
                  padding: "35px 30px",
                  minHeight: "220px",
                  boxShadow: "0px 10px 30px #D3D7DE99",
                  backgroundColor: "#fff",
                }}
              >
                <img src="images/augAnalytics.png" />
                <h5
                  style={{ paddingTop: "10px" }}
                  className={
                    GlobalCss.PoppinsSemiBold +
                    " " +
                    GlobalCss.H18 +
                    " " +
                    "card-text"
                  }
                >
                  Augmented analytics
                </h5>
                <p
                  className={
                    GlobalCss.PoppinsRegular +
                    " " +
                    GlobalCss.H12 +
                    " " +
                    "card-text"
                  }
                  style={{ color: "#8A92A3" }}
                >
                  We provide actionable insights for you to make quick and
                  effective decisions through visual forecasts and AI based key
                  influencers.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <OfferingSection content={offerContent} />

      <div>
        <div
          style={{
            margin: "0 auto",
            width: `${matches ? "95%" : "45%"}`,
            padding: "2%",
            textAlign: "center",
            paddingTop: "6rem",
            paddingBottom: "6rem",
          }}
        >
          <p
            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
            style={{ fontSize: `${matches ? "1.8rem" : "2.1rem"}` }}
          >
            Industries relevant to us
          </p>
          <p
            style={{ color: "#8A92A3" }}
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
          >
            We provide diverse IoT solutions in various sectors by decreasing
            human intervention to automate your business
          </p>
        </div>
        <div style={{}} className={GlobalCss.PoppinsRegular}>
          <div style={{}}>
            <Marquee pauseOnHover="true" speed="50">
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#383B62",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Food Supply Chain
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#D3D7DE",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Healthcare
              </p>
              <HtmlTooltip
                title={
                  // <React.Fragment>
                  //   <Typography color="inherit">Tooltip with HTML</Typography>
                  //   <em>{"And here's"}</em> <b>{"some"}</b>{" "}
                  //   <u>{"amazing content"}</u>. {"It's very engaging. Right?"}
                  // </React.Fragment>
                  <>
                  <div  style={{
                        paddingTop:"15px",
                        paddingBottom:"5px",
                        paddingLeft:"10px",
                        paddingRight:"10px"
                      }}>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24
                      }
                      style={{
                        color: "#6966C7",
                        opacity: "1",
                        paddingTop:"15px",
                        paddingBottom:"5px",
                        paddingLeft:"10px",
                        // paddingRight:"10px"
                      }}
                    >
                      Agriculture >
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                      style={{ color: "#8A92A3", opacity: "1",
                      paddingLeft:"10px",paddingRight:"10px" }}
                    >
                      Remotely control your farms through a single device.
                      Manage your entire crop life-cycle and produce an optimum
                      harvest.
                    </p>
                    </div>
                  </>
                }
              >
                <p
                  style={{
                    margin: "0 30px",
                    width: "max-content",
                    color:"#383B62",
                  }}
                  className={
                    GlobalCss.PoppinsRegular +
                    " " +
                    `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                  }
                >
                  Agriculture
                </p>
              </HtmlTooltip>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#D3D7DE",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Restaurant
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#383B62",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Retail
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#D3D7DE",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Public Sector
              </p>
              {/* <Tooltip
                title={longText}
                style={{ backgroundColor: "red !important" }}
              >
                
              </Tooltip>

               */}
            </Marquee>
          </div>
        </div>
        <div></div>
        <div style={{ marginTop: "4%" }} className={GlobalCss.PoppinsRegular}>
          <div style={{}}>
            <Marquee pauseOnHover="true" speed="50">
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#383B62",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Education
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#D3D7DE",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Pharmaceutical
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#383B62",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Commercial real estate
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#D3D7DE",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Aviation
              </p>
              <p
                style={{
                  margin: "0 30px",
                  width: "max-content",
                  color: "#383B62",
                }}
                className={
                  GlobalCss.PoppinsRegular +
                  " " +
                  `${matches ? GlobalCss.H18 : GlobalCss.H34}`
                }
              >
                Manufacturing
              </p>
            </Marquee>
          </div>
        </div>
      </div>
      <Testimonial />
    </>
  );
};

export default Home;
