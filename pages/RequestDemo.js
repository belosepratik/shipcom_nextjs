import { Button } from "@material-ui/core";
import React from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Layout from "../components/Layout";
import GlobalCss from "./css/Global.module.css";

const RequestDemo = () => {
  return (
    <>
      <Header />
      <div className="container">
        <div
          style={{
            background: "#FFFFFF 0% 0% no-repeat, padding-box",
            marginBottom: "0%",
            boxShadow: "0px 10px 30px #D3D7DE66",
            padding: "2rem",
          }}
        >
          <h1
            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
            style={{ textAlign: "center" }}
          >
            Get Started With Us
          </h1>
          <h5
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
            style={{ textAlign: "center", fontSize: "14px" }}
          >
            Help us with a few details and we will have a product
            <br /> expert reach out to you within 48 hourse!
          </h5>
          <div>
            <div
              className="form-sec"
              style={{
                width: "40%",
                margin: "0 auto",
                backgroundColor: "#FFFFFF",
                fontSize: "14px",
              }}
            >
              <form>
                <div class="form-group">
                  <label
                    className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  >
                    Name:
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="name"
                    placeholder=""
                    name="name"
                  />
                </div>
                <div class="form-group">
                  <label
                    className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  >
                    Email:
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="name"
                    placeholder=""
                    name="email"
                  />
                </div>

                <div class="form-group">
                  <label
                    className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  >
                    Phone No.:
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="phone"
                    placeholder=""
                    name="phone"
                  />
                </div>

                <div class="form-group">
                  <label
                    className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    for="inputState"
                  >
                    Product
                  </label>
                  <select id="inputState" class="form-control">
                    <option selected>Select options</option>
                    <option>...</option>
                  </select>
                </div>

                <div class="form-group">
                  <label
                    className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  >
                    Leave a comment:
                  </label>
                  <textarea
                    name="issues"
                    class="form-control"
                    id="iq"
                    placeholder=""
                  ></textarea>
                </div>

                <Button
                  fullWidth
                  style={{ textTransform: "none" }}
                  variant="contained"
                  style={{ color: "#fff", backgroundColor: "#6966C7" }}
                >
                  Submit
                </Button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <Footer />
    </>
  );
};

export default RequestDemo;
