import { Button } from "@material-ui/core";
import Image from "next/image";
import React from "react";
import Header from "../components/Header";
import Colors from "../styles/Colors";
import Footer from "../components/Footer";
import styles from "./css/FoodAndBeverage.module.css";
import IntroSection from "../components/Industry/IntroSection";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import DescribeSection from "../components/Industry/DescribeSection";
import OfferingSection from "../components/Industry/OfferingSection";
import HowSection from "../components/Industry/HowSection";
import Testimonial from "../components/Industry/Testimonial";
import GlobalCss from "./css/Global.module.css";

const Pharma = () => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");

  // console.log(isTabletOrMobileDevice);

  const headerContent = {
    title: (
      <>
        <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
          Smarter <span style={{ color: Colors.orange }}>Pharmaceutical</span>
          <br />
          manufacturing{isTabletOrMobileDevice && <br />} management
        </p>
      </>
    ),
    subTitle: (
      <p 
      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
        Improved manufacturing automation and production compliance
        {!isTabletOrMobileDevice && <br />} of temperature-sensitive medications
        and vaccines
      </p>
    ),

    image: "/images/pharma.PNG",
  };

  const describeContent = {
    describe1: "Preventative maintenance",
    subDescribe1:
      "Fitness bands, smartphones and other wirelessly connected devices give patients access to remote, yet personalized attention by doctors who can monitor calorie count, blood pressure variations & much more in real-time.",
    describe2: "Seamless supply chain",
    subDescribe2:
      "Smart sensors lead to an in-house patient flow management system to effectively track patient and medical equipment status, location & treatment schedule.",
    describe3: "Control manufacturing environment",
    subDescribe3:
      "Shipcom allows healthcare professionals to be more watchful and connect with the patients proactively. Data collected from our smart sensors can help physicians identify the best treatment process for patients.",
  };

  const offerContent = {
    title: "Our offerings",
    subTitle: (
      <>
        We offer the best-in-class IoT solutions to
        {isTabletOrMobileDevice && <br />} automate your business
        {!isTabletOrMobileDevice && <br />} backed by powerful and value-driven
        insights
      </>
    ),
  };

  return (
    <>
      <Header />

      <IntroSection content={headerContent} />

      <DescribeSection content={describeContent} />

      <OfferingSection content={offerContent} />

      <HowSection />

      <Testimonial />
    </>
  );
};

export default Pharma;
