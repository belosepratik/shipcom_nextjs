import Image from "next/image";
import React from "react";
import Header from "../components/Header";
import Layout from "../components/Layout";
import Colors from "../styles/Colors";

import Flickity from "react-flickity-component";

import styles from "./css/JobDetails.module.css";
import { BgAboutUs, RightArrowIcon } from "../assets/Icons/Icon";
import { Button } from "@material-ui/core";
import GlobalCss from "./css/Global.module.css";

export default function JobDetails() {
  return (
    <>
      <section className="main_job">
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="job_heading" id={styles.job_heading}>
                <h1
                //   className="text"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34+ " " +"text"}
                >
                  Job Details
                </h1>
              </div>
              <div className="sub_heading" id={styles.sub_heading}>
                <p
                //   className="text_heading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18 + " " +"text_heading"}
                  id={styles.text_heading}
                >
                  About us
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14 + " " +"text_para"}
                  id={styles.text_para}
                >
                  At Shipcom, we build the infrastructure necessary to measure
                  how people use space. The result of distributing this platform
                  is lower emissions, less waste, better access, safer
                  buildings, and better designed cities. It is a long term
                  pursuit and one we could use your help achieving.
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"text_para"}
                  id={styles.text_para}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p>
              </div>
              <div className="sub_heading" id={styles.sub_heading}>
                <p
                //   className="text_heading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18+ " " +"text_heading"}
                  id={styles.text_heading}
                >
                  What are we looking for?
                </p>
                <p
                //   className="text_subheading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14+ " " +"text_subheading"}
                  id={styles.text_subheading}
                >
                  1. Sed ut perspiciatis unde omnis iste natus error
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"text_para"}
                  id={styles.text_para}
                >
                  Sit voluptatem accusantium doloremque laudantium, totam rem
                  aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                  architecto beatae vitae dicta sunt explicabo.
                </p>
                <p
                //   className="text_subheading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14+ " " +"text_subheading"}
                  id={styles.text_subheading}
                >
                  2. Sed ut perspiciatis unde omnis iste natus error
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"text_para"}
                  id={styles.text_para}
                >
                  Sit voluptatem accusantium doloremque laudantium, totam rem
                  aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                  architecto beatae vitae dicta sunt explicabo.
                </p>
                <p
                //   className="text_subheading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14+ " " +"text_subheading"}
                  id={styles.text_subheading}
                >
                  3. Sed ut perspiciatis unde omnis iste natus error
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"text_para"}
                  id={styles.text_para}
                >
                  Sit voluptatem accusantium doloremque laudantium, totam rem
                  aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                  architecto beatae vitae dicta sunt explicabo.
                </p>
                <p
                //   className="text_subheading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14+ " " +"text_subheading"}
                  id={styles.text_subheading}
                >
                  4. Sed ut perspiciatis unde omnis iste natus error
                </p>
                <p
                //   className="text_para"
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"text_para"}
                  id={styles.text_para}
                >
                  Sit voluptatem accusantium doloremque laudantium, totam rem
                  aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                  architecto beatae vitae dicta sunt explicabo.
                </p>
              </div>
              <div className="sub_heading" id={styles.sub_heading}>
                <p
                //   className="text_heading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18+ " " +"text_heading"}
                  id={styles.text_heading}
                >
                  About the role
                </p>
                <div className="sub_ul" id={styles.sub_ul}>
                  <ul className="nested_ul" id={styles.nested_ul}>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Sed ut perspiciatis unde omnis iste natus error sit
                      voluptatem accusantium doloremque laudantium, totam rem
                      aperiam, eaque ipsa quae ab illo inventore
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Et quasi architecto beatae vitae dicta sunt explicabo.
                      Nemo enim ipsam voluptatem quia voluptas sit aspernatur
                      aut odit aut fugit, sed quia consequuntur magni dolores
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Qui ratione voluptatem sequi nesciunt. Neque porro
                      quisquam est, qui dolorem ipsum quia dolor sit amet,
                      consectetur, adipisci velit, sed quia non
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Numquam eius modi tempora incidunt ut labore et dolore
                      magnam aliquam quaerat voluptatem. Ut enim ad minima
                      veniam, quis nostrum exercitationem
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Ullam corporis suscipit laboriosam, nisi ut aliquid ex ea
                      commodi consequatur? Quis autem vel eum iure reprehenderit
                      qui in ea voluptate velit esse quam nihil
                    </li>
                  </ul>
                </div>
              </div>
              <div className="sub_heading" id={styles.sub_heading}>
                <p
                  className="text_heading"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18+ " " +"text_heading"}
                  id={styles.text_heading}
                >
                  Benefits
                </p>
                <div className="sub_ul" id={styles.sub_ul}>
                  <ul className="nested_ul" id={styles.nested_ul}>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Sed ut perspiciatis unde omnis iste natus error sit
                      voluptatem accusantium doloremque laudantium, totam rem
                      aperiam, eaque ipsa quae ab illo inventore
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Et quasi architecto beatae vitae dicta sunt explicabo.
                      Nemo enim ipsam voluptatem quia voluptas sit aspernatur
                      aut odit aut fugit, sed quia consequuntur magni dolores
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Qui ratione voluptatem sequi nesciunt. Neque porro
                      quisquam est, qui dolorem ipsum quia dolor sit amet,
                      consectetur, adipisci velit, sed quia non
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Numquam eius modi tempora incidunt ut labore et dolore
                      magnam aliquam quaerat voluptatem. Ut enim ad minima
                      veniam, quis nostrum exercitationem
                    </li>
                    <li
                    //   className="nested_li"
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14+ " " +"nested_li"}
                    >
                      Ullam corporis suscipit laboriosam, nisi ut aliquid ex ea
                      commodi consequatur? Quis autem vel eum iure reprehenderit
                      qui in ea voluptate velit esse quam nihil
                    </li>
                  </ul>
                </div>
              </div>
              <div className="sub_heading" id={styles.sub_heading}>
                <div className="button" id={styles.button}>
                  {/* <!-- <p className="btn_text">Apply for the job</p> --> */}
                  <button
                    type="button"
                    className="btn btn-primary"
                    id={styles.btn_primary}
                  >
                    <span
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                      style={{ color: "#fff" }}
                    >
                      Apply for the job
                    </span>
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card" id={styles.card} style={{ width: "18rem" }}>
                {/* <!-- <img className="card-img-top" src="..." alt="Card image cap"> --> */}
                <div className="card-body">
                  <h5 className="card-title" className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }id={styles.card_title}>
                    Account associate
                  </h5>
                </div>
                <div style={{ padding: "0% 10%" }}>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item" id={styles.listed_group_item}>
                      Sales
                    </li>
                    <li className="list-group-item" id={styles.listed_group_item}>
                      Full-time
                    </li>
                    <li className="list-group-item" id={styles.listed_group_item}>
                      Houston, Texas
                    </li>
                  </ul>
                </div>
                <div className="btns" id={styles.btns}>
                  <div className="button">
                    {/* <!-- <p className="btn_text">Apply for the job</p> --> */}
                    <button
                      type="button"
                      className="btn btn-primary"
                      id={styles.btn_primary}
                    >
                      Apply for the job
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
