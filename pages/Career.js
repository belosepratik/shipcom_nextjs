import { Avatar, Button } from "@material-ui/core";
import React from "react";
import Layout from "../components/Layout";
import Flickity from "react-flickity-component";
import styles from "./css/Career.module.css";
import Image from "next/image";
import Colors from "../styles/Colors";
import { RightArrowIcon, ZipZackBg } from "../assets/Icons/Icon";
import Header from "../components/Header";
import Footer from "../components/Footer";
import GlobalCss from "./css/Global.module.css";

const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`;
};

const data = [
  {
    name: "Jonathan Doe",
    desc:
      "It's a great place to work at, its organised, simple and I love their office personally",
    occupation: "HUMAN RESOURCES",
  },
  {
    name: "Jonathan Doe",
    desc:
      "It's a great place to work at, its organised, simple and I love their office personally",
    occupation: "HUMAN RESOURCES",
  },
  {
    name: "Marie John",
    desc:
      "It's a great place to work at, its organised, simple and I love their office personally",
    occupation: "ENGINEER",
  },
  {
    name: "Mathew Boat",
    desc:
      "It's a great place to work at, its organised, simple and I love their office personally",
    occupation: "DESIGNER",
  },
];

const job_opening_data = [
  {
    title: "Marketing",
    desc: "2 openings available",
  },
  {
    title: "Design",
    desc: "2 openings available",
  },
  {
    title: "Engineering",
    desc: "2 openings available",
  },
  {
    title: "Development",
    desc: "2 openings available",
  },
  {
    title: "Product",
    desc: "2 openings available",
  },
  {
    title: "Sales",
    desc: "2 openings available",
  },
  {
    title: "Marketing",
    desc: "2 openings available",
  },
  {
    title: "Design",
    desc: "2 openings available",
  },
  {
    title: "Engineering",
    desc: "2 openings available",
  },
];

const globe_data = [
  {
    title: "Houston, Texas",
    address: "11200 Richmond Ave, Houston, TX 77082.",
    phone: "Ph: (281) 558-5252",
    email: "E:sales@shipcomwireless.com",
  },
  {
    title: "Houston, Texas",
    address: "11200 Richmond Ave, Houston, TX 77082.",
    phone: "Ph: (281) 558-5252",
    email: "E:sales@shipcomwireless.com",
  },
  {
    title: "Houston, Texas",
    address: "11200 Richmond Ave, Houston, TX 77082.",
    phone: "Ph: (281) 558-5252",
    email: "E:sales@shipcomwireless.com",
  },
];

function Career() {
  const flickityOptions = {
    groupCells: true,
    cellAlign: "left",
    pageDots: false,
    lazyLoad: true,
    prevNextButtons: false,
  };

  return (
    <>
      <Header />

      <div className="container-fliud">
        <div className="container">
          <div className="row" className={styles.career_title}>
            <div>
              <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
                At Shipcom you can build
              </h1>
              <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
                an impactful <span style={{ color: "orange" }}>career </span>
                with us!
              </h1>
              <br />
              <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                Whether fresh or seasoned, we're always eager to meet <br />
                talent, so check out our open positions
              </p>
              <br />
              <Button
                style={{ textTransform: "none" }}
                variant="contained"
                color="primary"
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
              >
                See job openings
              </Button>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid">
        <div class="row">
          <img src="/images/career_group.png" />
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-6" style={{ padding: "6rem" }}>
            <div>
              <Image
                src="/images/career_philosophy.png"
                // layout="fill"
                alt="images"
                // style={{ reize }}
                height={100}
                width={300}
                resizeMode="cover"
              />
            </div>

            <div style={{ textAlign: "left" }}>
              <h1  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Our philosophy</h1>
              <br />

              <div>
                <p  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{  color: "#8A92A3" }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6" style={{ padding: "5rem" }}>
            <Image
              src="/images/career_office_grp.png"
              width={500}
              height={500}
            />
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <Flickity
              id="carousel"
              className={styles.carousel__main} // default ''
              elementType={"div"} // default 'div'
              options={flickityOptions} // takes flickity options {}
              disableImagesLoaded={false} // default false
              reloadOnUpdate // default false
              static // default false
            >
              {data.map((item, index) => {
                return (
                  <div
                    style={{
                      paddingLeft: "2rem",
                      paddingTop: "3rem",
                      paddingBottom: "8rem",
                    }}
                  >
                    <div className={styles.post}>
                      <div className={styles.post__header}>
                        <Avatar />
                        <div className="post__info">
                          <h4
                            style={{
                              color: "#eda243",
                              textDecoration: "uppercase", 
                            }}
                            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12}
                          >
                            {item.occupation}
                          </h4>
                          <h3  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18} > {item.name} </h3>
                          <p  className={GlobalCss.PoppinsRegular+ " " + GlobalCss.H12} style={{   color: "#8A92A3" }}>
                            {item.desc}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </Flickity>
          </div>
        </div>
      </div>

      <div className="container-fluid" style={{ backgroundColor: "#FAFAFB" }}>
        <div className="row" className={styles.career__commandments}>
          <div
            className="col-md-12"
            style={{ textAlign: "center", color: "#fff", padding: 5 + "rem" }}
          >
            <h1  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Work commandments of Shipcom</h1>
            <br />
            <p  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
              Lorem Ipsum is simply dummy text of the printing. <br />
              It is a long established fact that a reader
            </p>

            <div className={styles.wrapper}>
              <div className={styles.btn}>
                <button
                  type="button"
                  style={{
                    background: "none",
                    color: "#fff",
                    width: "200px",
                    height: "50px",
                    border: `1px solid #fff`,
                    fontSize: "18px",
                    borderRadius: "4px",
                    transition: "0.6s",
                    overflow: "hidden",
                  }} 
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14 + " " + styles.button}
                >
                  Join our team
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div
            className="col-md-12"
            style={{ width: "70%", margin: "-10% auto 0% auto" }}
          >
            <div
              className="row"
              style={{ backgroundColor: "white", padding: "5% 0%" }}
            >
              <div className="col-lg-6 col-md-6 col-sm-6" id={styles.contents}>
                <h2  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>Curiosity</h2>
                <p
                  style={{
                    width: "20rem",
                    textAlign: "center",
                    color: "#8A92A3", 
                  }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </p>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6" id={styles.contents}>
                <h2 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>Transparency</h2>
                <p
                  style={{
                    width: "20rem",
                    textAlign: "center",
                    color: "#8A92A3", 
                  }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </p>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6" id={styles.contents}>
                <h2 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>Collaboration</h2>
                <p
                  style={{
                    width: "20rem",
                    textAlign: "center",
                    color: "#8A92A3", 
                  }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </p>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6" id={styles.contents}>
                <h2 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>Quality</h2>
                <p
                  style={{
                    width: "20rem",
                    textAlign: "center",
                    color: "#8A92A3", 
                  }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="container-fluid"
        style={{ backgroundColor: "#FAFAFB", padding: "10% 0%" }}
      >
        <div className="container">
          <div className="row">
            <h3  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Perks & benefits</h3>
            <p   className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
              You won't only be working ut we'll ensure you enjoy
              <p style={{ fontSize: "14px" }}>all of these too</p>
            </p>
          </div>
          <div
            className="row"
            style={{ padding: "2% 0%", borderBottom: "2px solid #f1f2f4" }}
          >
            <div className="col-md-6">
              <span
                style={{
                  color: "#383B62",  
                }}  
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
              >
                Work from anywhere
              </span>
            </div>
            <div className="col-md-6">
              <p style={{ color: "#8A92A3"}}  
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                We are a 100% distributed team.
              </p>
            </div>
          </div>
          <div
            className="row"
            style={{ padding: "2% 0%", borderBottom: "2px solid #f1f2f4" }}
          >
            <div className="col-md-6">
              <span
                style={{
                  color: "#383B62", 
                }}
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
              >
                Work from anywhere
              </span>
            </div>
            <div className="col-md-6">
              <p style={{ color: "#8A92A3"}} 
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                Generous medical and dental for both you and your dependents.
              </p>
            </div>
          </div>
          <div
            className="row"
            style={{ padding: "2% 0%", borderBottom: "2px solid #f1f2f4" }}
          >
            <div className="col-md-6">
              <span
                style={{
                  color: "#383B62", 
                }}
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
              >
                Work from anywhere
              </span>
            </div>
            <div className="col-md-6">
              <p style={{ color: "#8A92A3"}} 
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                3 months paid maternity/paternity leave for US employees.
              </p>
            </div>
          </div>
          <div
            className="row"
            style={{ padding: "2% 0%", borderBottom: "2px solid #f1f2f4" }}
          >
            <div className="col-md-6">
              <span
                style={{
                  color: "#383B62", 
                }}
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
              >
                Work from anywhere
              </span>
            </div>
            <div className="col-md-6">
              <p style={{ color: "#8A92A3"}} 
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                Monthly coffee budget so you’re not just loitering at coffee
                shops.
              </p>
            </div>
          </div>
          <div
            className="row"
            style={{ padding: "2% 0%", borderBottom: "2px solid #f1f2f4" }}
          >
            <div className="col-md-6">
              <span
                style={{
                  color: "#383B62", 
                }}
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
              >
                Work from anywhere
              </span>
            </div>
            <div className="col-md-6">
              <p style={{ color: "#8A92A3"}} 
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
                We cover books, conferences, courses… get smarter!
              </p>
            </div>
          </div>
        </div>
      </div>

      <div
        className="container"
        style={{ backgroundColor: "white", padding: "10% 0%" }}
      >
        <div className="row">
          <div className="col-md-12" style={{ textAlign: "center" }}>
            <h1 
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Job openings</h1>
            <p 
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>
              We’ve got career opportunities across the board. Think you can fit
              in?
            </p>
            <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>Hop on!</p>
          </div>
        </div>

        <div className="row" style={{ paddingTop: "5%" }}>
          {job_opening_data.map((item, index) => {
            return (
              <div className="col-md-4">
                <div
                  style={{
                    textAlign: "left",
                    margin: "3%",
                    height: "7rem",
                    display: "flex",
                    background: "#fff",
                    borderRadius: "7px",
                    justifyContent: "space-between",
                    boxShadow: "5px 5px 50px -10px rgba(0, 0, 0, 0.25)",
                  }}
                >
                  <div style={{ padding: "2rem" }}>
                    <h4 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>{item.title}</h4>
                    <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12} style={{ color: Colors.green }}>2 openings available</p>
                  </div>
                  <div style={{ padding: "10px" }}>
                    <RightArrowIcon />
                  </div>
                </div>
              </div>
            );
          })}
        </div>

        <div className="row" style={{ textAlign: "center" }}>
          <div className={styles.wrapper}>
            <div className={styles.btn}>
              <button
                type="button"
                style={{
                  background: "none",
                  color: Colors.blue,
                  width: "170px",
                  height: "40px",
                  border: `1px solid ${Colors.blue}`, 
                  borderRadius: "4px",
                  transition: "0.6s",
                  overflow: "hidden",
                }} 
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14 + " " + styles.button}
              >
                View all openings
              </button>
            </div>
          </div>
        </div>
      </div>

      <div
        className="container"
        style={{ backgroundColor: "white", padding: "5% 0%" }}
      >
        <div className="row" style={{ textAlign: "center" }}>
          <div className="col-md-12">
            <h1  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Our offices</h1>
            <p  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>Get to know if we're functioning in or around you.</p>
          </div>
        </div>
        <div className="row" style={{ textAlign: "center" }}>
          <div className="col-md-12">
            <Image src="/images/globe.png" width={520} height={500} />
          </div>
        </div>
        <div className="row" style={{ textAlign: "center", width: "100%" }}>
          {globe_data.map((item, index) => {
            return (
              <div className="col-md-4">
                <div
                  key={index}
                  id={styles.globe__contents}
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div
                    style={{
                      paddingTop: "1rem",
                      paddingLeft: "1rem",
                      paddingRight: "5px",
                    }}
                  ></div>
                  <div
                    style={{
                      paddingTop: "1rem",
                      paddingRight: "1rem",
                      paddingBottom: "1rem",
                    }}
                  >
                    <h3  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>{item.title}</h3>
                    <p  className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}>{item.address}</p>
                    <br />
                    <p className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}>{item.phone}</p>
                    <u className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}>{item.email}</u>
                  </div>
                  <div style={{ padding: "7px" }}>
                    <RightArrowIcon />
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>

      <div
        className="container"
        style={{ backgroundColor: "", padding: "10% 0%" }}
      >
        <div className="row" style={{ textAlign: "center" }}>
          <div className="col-md-12">
            <h1 className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>Let's work together!</h1>
          </div>
          <div className="col-md-12">
            <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}>Give us a problem and we'll find a solution for you.</p>
          </div>
          <div className="col-md-12">
            <Button
              style={{ textTransform: "none" }}
              variant="contained"
              color="primary" className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
            >
              Request a demo
            </Button>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
}

export default Career;
