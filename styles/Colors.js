const white = "#fff";
const ghostWhite = "#FAFAFB";
const brightOrange = "#EDA343";
const blue = "#6966C7";
const denim = "#2C2E4B";
const darkBlue = "#1E1F31";
const lightDenim = "#383B62";
const green = "#84D49D";
const orange = "#EDA343";
const lightOrange = "#EDA343";
const lightGray = "#FFFFFF73";
const gray = "#8A92A3";

const Colors = {
  white,
  ghostWhite,
  brightOrange,
  blue,
  denim,
  darkBlue,
  lightDenim,
  green,
  orange,
  lightOrange,
  lightGray,
  gray,
};

export default Colors;
