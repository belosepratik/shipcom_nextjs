const PoppinsRegular = {
  fontFamily: "Poppins, sans-serif",
};

const Fonts = {
  PoppinsRegular,
};

export default Fonts;
