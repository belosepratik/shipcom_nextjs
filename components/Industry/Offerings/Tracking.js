import React from "react";
import styles from "../../../pages/css/Index.module.css";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import GlobalCss from "../../../pages/css/Global.module.css";

function Tracking({ track }) {
  const matches = useMediaQuery("(max-width:480px)");

  return (
    <>
      {track === 31 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <div className="col-12">
                <p
                  className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
                  style={{
                    background: "#6EBC86",
                    borderRadius: 27,
                    width: "max-content",
                    padding: "7px 10px",
                    color: "#fff",
                    marginLeft: "12%",
                  }}
                >
                  #Education
                </p>
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  marginBottom: 30,
                  justifyContent: "center",
                }}
              >
                <img
                  src="images/tracking1TotalOccupants.png"
                  style={{ margin: "0 10px" }}
                />
                <img
                  src="images/tracking1PeakHour.png"
                  style={{ margin: "0 10px" }}
                />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  marginBottom: 30,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <img
                  src="images/tracking1CapacityUsed.png"
                  style={{ margin: "0 20px 0 0" }}
                />
                <img src="images/healthcare.png" style={{ margin: "0 20px" }} />
              </div>
              <div className="col-12">
                <img src="images/tracking1MainImage.png" />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <img src="images/tracking1MainImageMobile.png" />
            </div>
          </div>
        </>
      )}
      {track === 32 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <div
                className="col-6"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                }}
              >
                <div style={{ marginBottom: 40 }}>
                  <img src="images/commRealEstate.png" />
                </div>
                <div
                  style={{
                    marginBottom: 30,
                  }}
                >
                  <img src="images/tracking2AvgVisit.png" />
                </div>
                <div
                  style={{
                    marginBottom: 30,
                  }}
                >
                  <img src="images/tracking2Newoccupants.png" />
                </div>
                <div
                  style={{
                    marginBottom: 30,
                  }}
                >
                  <img src="images/tracking2ReturningOccupants.png" />
                </div>
              </div>
              <div
                className="col-6"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                }}
              >
                <div style={{ alignSelf: "center", margin: "30px 0" }}>
                  <img src="images/healthcare.png" />
                </div>
                <div style={{ marginBottom: 40 }}>
                  <img src="images/pharmaceuticals.png" />
                </div>
                <div style={{ marginBottom: 30 }}>
                  <img src="images/goodAirQuality.png" />
                </div>
                <div style={{ marginBottom: 30 }}>
                  <img src="images/tracking2SqFtPerPerson.png" />
                </div>
              </div>
              <div
                className="col-12"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <img src="images/15totaloccupants.png" />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <div className="col-12" style={{ marginBottom: 20 }}>
                <img src="images/commRealEstate.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "space-evenly",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/tracking2Newoccupants.png" />
                <img src="images/retail.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "space-evenly",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/healthcare.png" />
                <img src="images/goodAirQuality.png" />
              </div>
            </div>
          </div>
        </>
      )}
      {track === 33 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <div
                className="col_12"
                style={{
                  flexDirection: "row",
                  marginBottom: 30,
                  justifyContent: "space-evenly",
                  display: "flex",
                }}
              >
                <img src="images/restaurant.png" />
                <img src="images/pharmaceuticals.png" />
                <img src="images/healthcare.png" />
              </div>
              <div className="col-12">
                <img src="images/tracking3.png" style={{ width: "100%" }} />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <img src="images/tracking3ImageMobile.png" />
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default Tracking;
