import React from "react";
import styles from "../../../pages/css/Index.module.css";
import styles2 from "../../../pages/css/Offering.module.css";
import GlobalCss from "../../../pages/css/Global.module.css";
import useMediaQuery from "@material-ui/core/useMediaQuery";

function ColdChain({ cold }) {
  const matches = useMediaQuery("(max-width:480px)");

  return (
    <>
      {cold === 41 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <div className="col-md-6" style={{ marginBottom: 40 }}>
                <img src="images/tempAlerts.png" />
              </div>
              <div
                className="col-md-6"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  marginBottom: 40,
                }}
              >
                <img src="images/foodandbev.png" />
                <img
                  src="images/restaurant2.png"
                  style={{ alignSelf: "center" }}
                />
                <img src="images/healthcare.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  marginBottom: 30,
                  justifyContent: "space-between",
                }}
              >
                <img
                  src="images/coldChain1vanTemp.png"
                  style={{ margin: "0 10px" }}
                />
                <img
                  src="images/coldChain1vanTemp.png"
                  style={{ margin: "0 10px" }}
                />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  marginBottom: 30,
                  justifyContent: "flex-end",
                }}
              >
                <img
                  src="images/coldChain1totalTrip.png"
                  style={{ margin: "0 20px" }}
                />
                <img
                  src="images/coldChain1totalAlerts.png"
                  style={{ margin: "0 20px" }}
                />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <div
                className="col-12"
                style={{
                  marginBottom: 20,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                }}
              >
                <img src="images/foodandbev.png" />
              </div>
              <div
                className="col-12"
                style={{
                  marginBottom: 20,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-start",
                }}
              >
                <img src="images/coldChain1vanTemp.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "space-evenly",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/healthcare.png" />
                <img src="images/coldChain1totalAlerts.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/restaurant2.png" />
              </div>
            </div>
          </div>
        </>
      )}
      {cold === 42 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}`, marginTop: 40 }}
            >
              <div
                className="col-md-7"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                }}
              >
                <img
                  src="images/foodSupplyChain.png"
                  style={{ marginBottom: 30 }}
                />
                <img src="images/stop8.png" />
              </div>
              <div
                className="col-md-4"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                  justifyContent: "flex-end",
                }}
              >
                <img src="images/pharmaceuticals.png" />
              </div>
              <div className="col-md-1"></div>
            </div>
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}`, marginTop: 30 }}
            >
              <div
                className="col-md-4"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                  justifyContent: "center",
                }}
              >
                <img src="images/agriculture.png" />
              </div>
              <div
                className="col-md-7"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                }}
              >
                <img src="images/alertCount.png" />
              </div>
              <div className="col-md-1"></div>
            </div>
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}`, marginTop: 40 }}
            >
              <div
                className="col-md-12"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/vanBar.png" />
              </div>
              <div
                className="col-md-12"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <img src="images/coolerBar.png" />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <div
                className="col-12"
                style={{
                  marginBottom: 20,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <img src="images/retail.png" />
              </div>
              <div
                className="col-12"
                style={{
                  marginBottom: 20,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-evenly",
                }}
              >
                <img src="images/foodandbev.png" />
                <img src="images/alertCount2.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/stop8.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/pharmaceuticals.png" />
              </div>
            </div>
          </div>

          {/* <div class="col-sm">
            <div className="row">
              <img className="col-md-6" src="images/MaskGroup89.png" />
              <div className="col-md-6">
                <p>2nd COLD</p>
              </div>
            </div>
            <div className="row">
              <img src="images/MaskGroup90.png" style={{ width: "100%" }} />
              <img src="images/MaskGroup90.png" style={{ width: "100%" }} />
              <img src="images/MaskGroup90.png" style={{ width: "100%" }} />
            </div>
          </div> */}
        </>
      )}
      {cold === 43 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <img src="images/coldChain3.png" />
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <div
                className="col-12"
                style={{
                  marginBottom: 20,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-evenly",
                }}
              >
                <img src="images/foodandbev.png" />
                <img src="images/agriculture.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/coldChain3mainImageMobile.png" />
              </div>
              <div
                className="col-12"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <img src="images/pharmaceuticals.png" />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default ColdChain;
