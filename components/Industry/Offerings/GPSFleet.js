import React from "react";
import styles from "../../../pages/css/Index.module.css";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import GlobalCss from "../../../pages/css/Global.module.css";
import styles2 from "../../../pages/css/Offering.module.css";

const GPSFleet = ({ gps }) => {
  const matches = useMediaQuery("(max-width:480px)");

  console.log(gps);
  return (
    <>
      {gps === 21 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <img src="images/gps1.png" />
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <img src="images/gpsMainImageMobile.png" />
            </div>
          </div>
        </>
      )}
      {gps === 22 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <img src="images/gps2.png" />
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <div
                className="col-12"
                style={{ display: "flex", justifyContent: "space-evenly" }}
              >
                <img src="images/manufacturing.png" />
                <img src="images/commRealEstate.png" />
              </div>
              <div className="col-12" style={{ textAlign: "center" }}>
                <img src="images/gps2Mainmobile.png" />
              </div>
            </div>
          </div>
        </>
      )}
      {gps === 23 && (
        <>
          <div class="col-sm">
            {/* desktop version */}
            <div
              className="row"
              style={{ display: `${matches ? "none" : "flex"}` }}
            >
              <div className="col-12" style={{display:"flex",marginBottom:30,justifyContent:"space-around"}}>
                <img src="images/foodandbev.png" />
                <img src="images/agriculture.png" />
                <img src="images/aviation.svg" />
              </div>
              <div className="col-12">
              <img src="images/gps3mainimage.png" />
              </div>
            </div>
            {/* mobile version */}
            <div
              className="row"
              style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
            >
              <img src="images/gps3mainimagemobile.png" />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default GPSFleet;
