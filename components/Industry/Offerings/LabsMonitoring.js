import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import styles2 from "../../../pages/css/Offering.module.css";

const LabsMonitoring = ({ labs }) => {
  const matches = useMediaQuery("(max-width:480px)");
  return (
    <>
      {labs === 11 && (
        <div class="col-sm">
          {/* desktop version */}
          <div
            className="row"
            style={{ display: `${matches ? "none" : "block"}`, marginTop: 50 }}
          >
            <div
              className="col-12"
              style={{ textAlign: "center", marginBottom: 40 }}
            >
              <img src="images/commRealEstate.png" />
            </div>
            <div
              className="col-12"
              style={{
                display: "flex",
                justifyContent: "space-evenly",
                alignItems: "center",
                marginBottom: 40,
              }}
            >
              <img src="images/lab1latestvalue.png" />
              <img src="images/lab1avgvalue.png" />
              <img src="images/healthcare.png" />
            </div>
            <div
              className="col-12"
              style={{ textAlign: "center", marginBottom: 40 }}
            >
              <img src="images/lab1mainimage.png" />
            </div>
            <div
              className="col-12"
              style={{ textAlign: "right", width: "90%" }}
            >
              <img src="images/pharmaceuticals.png" />
            </div>
          </div>

          {/* mobile version */}
          <div
            className="row"
            style={{ display: `${matches ? "block" : "none"}`, marginTop: 50 }}
          >
            <div
              className="col-12"
              style={{
                display: "flex",
                justifyContent: "space-evenly",
                alignItems: "center",
                marginBottom: 30,
              }}
            >
              <img src="images/commRealEstate.png" />
              <img src="images/healthcare.png" />
            </div>
            <div
              className="col-12"
              style={{ textAlign: "center", marginBottom: 30 }}
            >
              <img src="images/lab1mainimagemobile.png" />
            </div>
            <div className="col-12" style={{ textAlign: "center" }}>
              <img src="images/pharmaceuticals.png" />
            </div>
          </div>
        </div>
      )}

      {/* toggle 12 */}

      {labs === 12 && (
        <div class="col-sm">
          {/* desktop version */}
          <div
            className="row"
            style={{ display: `${matches ? "none" : "flex"}`, marginTop: 60 }}
          >
            <div className="col-7" style={{ textAlign: "right" }}>
              <img
                src="images/pharmaceuticals.png"
                style={{ marginBottom: 35 }}
              />
              <img src="images/lab2humidty.png" />
            </div>
            <div
              className="col-5"
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
                alignItems: "flex-end",
              }}
            >
              <img src="images/lab2800ppm.png" />
              <img src="images/healthcare.png" />
              <img
                src="images/foodandbev.png"
                style={{ alignSelf: "center" }}
              />
            </div>
            <div
              className="col-12"
              style={{ textAlign: "center", marginTop: 50 }}
            >
              <img src="images/lab2temperature.png" />
            </div>
          </div>
          {/* mobile version */}
          <div
            className="row"
            style={{ display: `${matches ? "block" : "none"}`, marginTop: 50 }}
          >
            <div
              className="col-12"
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
              }}
            >
              <img src="images/healthcare.png" />
              <img src="images/lab2800ppm.png" />
            </div>
            <div className="col-12" style={{ textAlign: "center" }}>
              <img src="images/lab2humidty.png" />
            </div>
          </div>
        </div>
      )}

      {/* toggle 13 */}

      {labs === 13 && (
        <div class="col-sm">
          {/* desktop version */}
          <div
            className="row"
            style={{ display: `${matches ? "none" : "flex"}`, marginTop: 20 }}
          >
            <div className="col-12" style={{ textAlign: "center" }}>
              <img src="images/lab3mainimage.png" />
            </div>
          </div>
          {/* mobile version */}
          <div
            className="row"
            style={{ display: `${matches ? "flex" : "none"}`, marginTop: 50 }}
          >
            <div className="col-12" style={{ textAlign: "center" }}>
              <img src="images/lab3mainimagemobile.png" />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default LabsMonitoring;
