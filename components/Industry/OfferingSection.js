import { Button } from "@material-ui/core";
import React from "react";
import Colors from "../../styles/Colors";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import styles from "../../pages/css/FoodAndBeverage.module.css";
import DropDown from "../DropDown";
import LabsMonitoring from "./Offerings/LabsMonitoring";
import GlobalCss from "../../pages/css/Global.module.css";
import styles2 from "../../pages/css/Offering.module.css";
import GPSFleet from "./Offerings/GPSFleet";
import Tracking from "./Offerings/Tracking";
import ColdChain from "./Offerings/ColdChain";

const OfferingSection = ({ content }) => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");

  const [selected, setSelected] = React.useState(1);

  const [labs, setLabs] = React.useState(11);
  const [gps, setGps] = React.useState(21);
  const [track, setTrack] = React.useState(31);
  const [cold, setCold] = React.useState(41);

  const [toggleState, setToggleState] = React.useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  const dropdown = [
    {
      id: 4,
      value: "Cold chain logistics",
    },
    {
      id: 1,
      value: "Labs monitoring",
    },
    {
      id: 2,
      value: "GPS and fleet management",
    },
    {
      id: 3,
      value: "Occupancy & capacity tracking",
    },
  ];

  return (
    <>
      <div
        style={{
          background: "#2C2E4B",
          paddingTop: "4rem",
          paddingBottom: "4rem",
        }}
      >
        <div style={{ textAlign: "center" }}>
          <p
            style={{ color: Colors.white }}
            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
          >
            {content.title}
          </p>
          <p
            style={{ color: Colors.lightGray }}
            className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
          >
            {content.subTitle}
          </p>
        </div>
        <br />
        {isTabletOrMobileDevice ? (
          <DropDown
            items={dropdown}
            toggleState={toggleState}
            setToggleState={setToggleState}
          />
        ) : (
          <div style={{ padding: "3rem" }} className="container">
            <div
              style={{ background: "#21223B", borderRadius: "10px" }}
              className="row"
            >
              {toggleState === 1 ? (
                <div
                  style={{
                    color: Colors.lightGray,
                    padding: "10px",
                  }}
                  className="col-sm"
                >
                  <Button
                    variant="contained"
                    style={{
                      backgroundColor: "#6966C7",
                      textTransform: "none",
                    }}
                    fullWidth
                  >
                    <span
                      style={{ color: Colors.white }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    >
                      Labs monitoring
                    </span>
                  </Button>
                </div>
              ) : (
                <div
                  onClick={() => setToggleState(1)}
                  style={{
                    color: Colors.lightGray,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "10px",
                    cursor: "pointer",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <span
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    style={{ color: Colors.white }}
                  >
                    Labs monitoring
                  </span>
                </div>
              )}

              {toggleState === 2 ? (
                <div
                  style={{
                    color: Colors.lightGray,
                    // display: "flex",
                    // justifyContent: "flex-end",
                    padding: "10px",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <Button
                    variant="contained"
                    style={{
                      backgroundColor: "#6966C7",
                      textTransform: "none",
                    }}
                    fullWidth
                  >
                    <span
                      style={{ color: Colors.white }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    >
                      GPS and fleet management
                    </span>
                  </Button>
                </div>
              ) : (
                <div
                  onClick={() => setToggleState(2)}
                  style={{
                    color: Colors.lightGray,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "10px",
                    cursor: "pointer",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <span
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    style={{ color: Colors.white }}
                  >
                    GPS and fleet management
                  </span>
                </div>
              )}

              {toggleState === 3 ? (
                <div
                  style={{
                    color: Colors.lightGray,
                    // display: "flex",
                    // justifyContent: "flex-end",
                    padding: "10px",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <Button
                    variant="contained"
                    style={{
                      backgroundColor: "#6966C7",
                      textTransform: "none",
                    }}
                    fullWidth
                  >
                    <span
                      style={{ color: Colors.white }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    >
                      Occupancy & capacity tracking
                    </span>
                  </Button>
                </div>
              ) : (
                <div
                  onClick={() => setToggleState(3)}
                  style={{
                    color: Colors.lightGray,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "10px",
                    cursor: "pointer",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <span
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    style={{ color: Colors.white }}
                  >
                    Occupancy & capacity tracking
                  </span>
                </div>
              )}

              {toggleState === 4 ? (
                <div
                  style={{
                    color: Colors.lightGray,
                    // display: "flex",
                    // justifyContent: "flex-end",
                    padding: "10px",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <Button
                    variant="contained"
                    style={{
                      backgroundColor: "#6966C7",
                      textTransform: "none",
                    }}
                    fullWidth
                  >
                    <span
                      style={{ color: Colors.white }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    >
                      Cold chain logistics
                    </span>
                  </Button>
                </div>
              ) : (
                <div
                  onClick={() => setToggleState(4)}
                  style={{
                    color: Colors.lightGray,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "10px",
                    cursor: "pointer",
                  }}
                  className={
                    GlobalCss.PoppinsMedium +
                    " " +
                    GlobalCss.H13 +
                    " " +
                    "col-sm"
                  }
                >
                  <span
                    className={GlobalCss.PoppinsMedium + " " + GlobalCss.H13}
                    style={{ color: Colors.white }}
                  >
                    Cold chain logistics
                  </span>
                </div>
              )}
            </div>
          </div>
        )}

        <div className="container">
          <div class="row">
            {toggleState === 3 && (
              <>
                <div class="col-sm" id={styles.offers_content_left}>
                  <div>
                    <h3
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24
                      }
                    >
                      Occupancy & capacity tracking
                    </h3>

                    <span
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                      style={{ color: Colors.lightGray }}
                    >
                      Keep staff and customers safe with real-time alerts
                      whenever capacity levels approach dangerous thresholds in
                      various zones.
                    </span>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{
                        color: "#fff",
                        border: 1,
                        borderWidth: 1,
                        borderStyle: "solid",
                        borderColor: "#fff",
                        borderRadius: 7,
                        textDecoration: "none",
                        padding: "10px 20px",
                        cursor: "pointer",
                        width: "max-content",
                        marginTop:35
                      }}
                    >
                      Explore Offering
                    </p>
                  </div>
                  {isTabletOrMobileDevice ? (
                    <>
                      <div id={styles.offers_left_box}>
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: "space-around",
                            paddingTop: "3rem",
                          }}
                        >
                          <div
                            onClick={() => setTrack(31)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                track == 31 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setTrack(32)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                track == 32 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setTrack(33)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                track == 33 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                        </div>
                        <div>
                          {/* <img src="images/Group115.png" /> */}

                          <div>
                            <p
                              className={
                                GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                              }
                            >
                              Safeguard high-traffic spaces
                            </p>
                            <span
                              className={
                                GlobalCss.PoppinsRegular + " " + GlobalCss.H12
                              }
                              style={{ color: Colors.lightGray }}
                            >
                              comprehensive, real-time visibility of fleets,
                              refrigerators, freezeres, rides and schudules on a
                              screens.
                            </span>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <div id={styles.offers_left_box}>
                      <div>
                        {track == 31 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setTrack(31)}
                            style={{
                              color: `${track == 31 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Safeguard high-traffic spaces
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Keep staff and customers safe with real-time alerts
                            whenever capacity levels approach dangerous
                            thresholds in various zones.
                          </span>
                        </div>
                      </div>
                      <div>
                        {track == 32 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setTrack(32)}
                            style={{
                              color: `${track == 32 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Improve workplace experience
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Analyze occupancy data to release unoccupied rooms,
                            predict demand, test office configurations, and
                            improve individual experience.
                          </span>
                        </div>
                      </div>
                      <div>
                        {track == 33 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setTrack(33)}
                            style={{
                              color: `${track == 33 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Study space and dwell times
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Through the predictive analytics of capacity and
                            usage data, enable quick, confident recommendations
                            on space utilization.
                          </span>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
                {/* <div class="col-sm" id={styles.offers_right_box}>
                  <div className="row">
                    <img className="col-md-6" src="images/MaskGroup89.png" />
                    <div className="col-md-6">
                      <p>#FoodSupplychain</p>
                      <p>#Pharmaceuticals</p>
                      <p>#Agriculture</p>
                    </div>
                  </div>
                  <div className="row">
                    <img
                      src="images/MaskGroup90.png"
                      style={{ width: "100%" }}
                    />
                    <img
                      src="images/MaskGroup90.png"
                      style={{ width: "100%" }}
                    />
                    <img
                      src="images/MaskGroup90.png"
                      style={{ width: "100%" }}
                    />
                  </div>
                </div>
               */}
                <Tracking track={track} />
              </>
            )}

            {toggleState === 2 && (
              <>
                <div class="col-sm" id={styles.offers_content_left}>
                  <div>
                    <h3
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24
                      }
                    >
                      GPS and fleet management
                    </h3>

                    <span
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                      style={{ color: Colors.lightGray }}
                    >
                      GPS visibility of commercial vehicles with an advanced
                      fleet management application to boost efficiency and
                      productivity while ensuring compliance.
                    </span>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{
                        color: "#fff",
                        border: 1,
                        borderWidth: 1,
                        borderStyle: "solid",
                        borderColor: "#fff",
                        borderRadius: 7,
                        textDecoration: "none",
                        padding: "10px 20px",
                        cursor: "pointer",
                        width: "max-content",
                        marginTop:35
                      }}
                    >
                      Explore Offering
                    </p>
                  </div>
                  {isTabletOrMobileDevice ? (
                    <>
                      <div id={styles.offers_left_box}>
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: "space-around",
                            paddingTop: "3rem",
                          }}
                        >
                          <div
                            onClick={() => setGps(21)}
                            style={{ cursor: "pointer" }}
                            style={{
                              height: "4px",
                              width: "40px",

                              backgroundColor: `${
                                gps == 21 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setGps(22)}
                            style={{ cursor: "pointer" }}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                gps == 22 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setGps(23)}
                            style={{ cursor: "pointer" }}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                gps == 23 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                        </div>
                        <div>
                          {/* <img src="images/Group115.png" /> */}

                          <div>
                            <p
                              className={
                                GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                              }
                            >
                              Advanced mapping and telematics
                            </p>
                            <span
                              className={
                                GlobalCss.PoppinsRegular + " " + GlobalCss.H12
                              }
                              style={{ color: Colors.lightGray }}
                            >
                              comprehensive, real-time visibility of fleets,
                              refrigerators, freezeres, rides and schudules on a
                              screens.
                            </span>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <div id={styles.offers_left_box}>
                      <div>
                        {gps == 21 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setGps(21)}
                            style={{
                              color: `${gps == 21 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Advanced mapping and telematics
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Reliable sensors connect to GPS to predict traffic &
                            time of arrival enabling more efficient logistical
                            planning.
                          </span>
                        </div>
                      </div>
                      <div>
                        {gps == 22 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setGps(22)}
                            style={{
                              color: `${gps == 22 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Route planning and optimization
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Real-time alerts supporting users to take action,
                            ensuring deliveries, routes and schedules are
                            optimized.
                          </span>
                        </div>
                      </div>
                      <div>
                        {gps == 23 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setGps(23)}
                            style={{
                              color: `${gps == 23 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Maintenance and health status
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            Vehicle diagnostics analyzed to save on significant
                            operational costs and manage maintenance issues
                            remotely.
                          </span>
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                <GPSFleet gps={gps} />
              </>
            )}

            {toggleState === 1 && (
              <>
                <div class="col-sm" id={styles.offers_content_left}>
                  <div>
                    <h3
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24
                      }
                    >
                      Labs monitoring
                    </h3>

                    <span
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                      style={{ color: Colors.lightGray }}
                    >
                      Enable clinical staff and technicians to ensure lab
                      samples are maintained at set temperature ranges by taking
                      immediate action if storage conditions exceed set
                      parameters.
                    </span>
                    <p style={{marginTop:"35px"}}>
                      <a
                        className={
                          GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                        }
                        style={{
                          color: "#fff",
                          border: 1,
                          borderWidth: 1,
                          borderStyle: "solid",
                          borderColor: "#fff",
                          borderRadius: 7,
                          textDecoration: "none",
                          padding: "10px 20px",
                          cursor: "pointer",
                          width: "max-content",
                        }}
                      >
                        Explore Offering
                      </a>
                    </p>
                  </div>
                  {isTabletOrMobileDevice ? (
                    <>
                      <div id={styles.offers_left_box}>
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: "space-around",
                            paddingTop: "3rem",
                          }}
                        >
                          <div
                            onClick={() => setLabs(11)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                labs == 11 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setLabs(12)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                labs == 12 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setLabs(13)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                labs == 13 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                        </div>
                        <div>
                          {/* <img src="images/Group115.png" /> */}

                          <div>
                            <p
                              style={{ cursor: "pointer" }}
                              className={
                                GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                              }
                            >
                              Prevent product loss
                            </p>
                            <span
                              className={
                                GlobalCss.PoppinsRegular + " " + GlobalCss.H14
                              }
                              style={{ color: Colors.lightGray }}
                            >
                              Users can view refrigeration groups and receive
                              alerts when units are outside of their monitored
                              threshold.
                            </span>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <div id={styles.offers_left_box}>
                      <div>
                        {labs == 11 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setLabs(11)}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                            style={{
                              color: `${labs == 11 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                          >
                            Prevent product loss
                          </p>
                          <span
                            className={
                              GlobalCss.PoppinsRegular + " " + GlobalCss.H14
                            }
                            style={{ color: Colors.lightGray }}
                          >
                            Users can view refrigeration groups and receive
                            alerts when units are outside of their monitored
                            threshold.
                          </span>
                        </div>
                      </div>
                      <div>
                        {labs == 12 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setLabs(12)}
                            style={{
                              color: `${labs == 12 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Ambient condition monitoring
                          </p>
                          <span
                            className={
                              GlobalCss.PoppinsRegular + " " + GlobalCss.H14
                            }
                            style={{ color: Colors.lightGray }}
                          >
                            Monitor and electronically document a range of
                            environmental information such as temperature,
                            humidity, carbon dioxide levels.
                          </span>
                        </div>
                      </div>
                      <div>
                        {labs == 13 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setLabs(13)}
                            style={{
                              color: `${labs == 13 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Exceed compliance standards
                          </p>
                          <span
                            className={
                              GlobalCss.PoppinsRegular + " " + GlobalCss.H14
                            }
                            style={{ color: Colors.lightGray }}
                          >
                            Surpass temperature monitoring protocols and prepare
                            for potential audits by the Joint Commission using
                            best practices.
                          </span>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
                {/* toggle 11 */}
                <LabsMonitoring labs={labs} setLabs={setLabs} />
              </>
            )}

            {toggleState === 4 && (
              <>
                <div class="col-sm" id={styles.offers_content_left}>
                  <div>
                    <h3 className={GlobalCss.PoppinsSemiBold}>
                      Cold chain logistics
                    </h3>

                    <span style={{ color: Colors.lightGray }}>
                      Ensure temparatures and maintained at every step of the
                      supply chain to reduce risk of product loss and streamline
                      delivery.
                    </span>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12
                      }
                      style={{
                        color: "#fff",
                        border: 1,
                        borderWidth: 1,
                        borderStyle: "solid",
                        borderColor: "#fff",
                        borderRadius: 7,
                        textDecoration: "none",
                        padding: "10px 20px",
                        cursor: "pointer",
                        width: "max-content",
                        marginTop:35
                      }}
                    >
                      Explore Offering
                    </p>
                  </div>
                  {isTabletOrMobileDevice ? (
                    <>
                      <div id={styles.offers_left_box}>
                        <div
                          style={{
                            display: "flex",
                            // justifyContent: "space-around",
                            paddingTop: "3rem",
                          }}
                        >
                          <div
                            onClick={() => setCold(41)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                cold == 41 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setCold(42)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                cold == 42 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                          <div
                            onClick={() => setCold(43)}
                            style={{
                              height: "4px",
                              width: "40px",
                              backgroundColor: `${
                                cold == 43 ? "#EDA343" : "#434565"
                              }`,
                              margin: "4px",
                              borderRadius: "3px",
                            }}
                          ></div>
                        </div>
                        <div>
                          {/* <img src="images/Group115.png" /> */}

                          <div>
                            <p>Monitor cold chain operations</p>
                            <span style={{ color: Colors.lightGray }}>
                              comprehensive, real-time visibility of fleets,
                              refrigerators, freezeres, rides and schudules on a
                              screens.
                            </span>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <div id={styles.offers_left_box}>
                      <div>
                        {cold == 41 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setCold(41)}
                            style={{
                              color: `${cold == 41 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Monitor cold chain operations
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            comprehensive, real-time visibility of fleets,
                            refrigerators, freezeres, rides and schudules on a
                            screens.
                          </span>
                        </div>
                      </div>
                      <div>
                        {cold == 42 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setCold(42)}
                            style={{
                              color: `${cold == 42 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Monitor cold chain operations
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            comprehensive, real-time visibility of fleets,
                            refrigerators, freezeres, rides and schudules on a
                            screens.
                          </span>
                        </div>
                      </div>
                      <div>
                        {cold == 43 ? (
                          <>
                            <img src="images/Group115.png" />
                          </>
                        ) : (
                          <>
                            <img src="images/Ellipse 46.png" />
                          </>
                        )}
                        <div>
                          <p
                            onClick={() => setCold(43)}
                            style={{
                              color: `${cold == 43 ? "#EDA343" : "white"}`,
                              cursor: "pointer",
                            }}
                            className={
                              GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18
                            }
                          >
                            Monitor cold chain operations
                          </p>
                          <span style={{ color: Colors.lightGray }}>
                            comprehensive, real-time visibility of fleets,
                            refrigerators, freezeres, rides and schudules on a
                            screens.
                          </span>
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                {/* toggle 41 */}
                {/* 
                <div class="col-sm" id={styles.cold_chain}>
                  <div className="row" style={{marginBottom:40}}>
                    <div className="col-md-6">
                      <img src="images/tempAlerts.png" />
                    </div>
                    <div className="col-md-6" style={{display:"flex",alignItems:"flex-start",justifyContent:"space-between",flexDirection:"column",paddingLeft:10}}>
                      <img src="images/foodSupplyChain.png" />
                      <img src="images/pharmaceuticals.png" style={{paddingLeft:40}} />
                      <img src="images/agriculture.png" />
                    </div>
                  </div>
                  <div className="row">
                    <img src="images/supplyAir.png" style={{ width: "90%",marginBottom:20 }} />
                    <img
                      src="images/humidityBar.png"
                      style={{ width: "90%",marginBottom:20 }}
                    />
                    <img src="images/ambAirTemp.png" style={{ width: "90%",marginBottom:20 }} />
                  </div>
                </div> */}

                {/* {cold === 41 && (
                  <div class="col-sm" id={styles.cold_chain}>
                    <div
                      className="row"
                      style={{
                        marginBottom: 40,
                        alignItems: "baseline",
                        maxWidth: "90%",
                      }}
                    >
                      <div className="col-sm-12">
                        <img
                          src="images/foodSupplyChain.png"
                          style={{ marginBottom: 20, marginLeft: "30%" }}
                        />
                      </div>
                      <div
                        className="col-sm-12"
                        style={{
                          flexDirection: "row",
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "baseline",
                          marginBottom: "35px",
                        }}
                      >
                        <img src="images/stop8.png" />
                        <img src="images/pharmaceuticals.png" />
                      </div>
                      <div
                        className="col-sm-12"
                        style={{
                          flexDirection: "row",
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                          marginBottom: "35px",
                        }}
                      >
                        <div>
                          <img src="images/agriculture.png" />
                        </div>
                        <div>
                          <img src="images/alertCount.png" />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <img
                        src="images/supplyAir.png"
                        style={{ width: "90%", marginBottom: 20 }}
                      />
                      <img
                        src="images/humidityBar.png"
                        style={{ width: "90%", marginBottom: 20 }}
                      />
                      <img
                        src="images/ambAirTemp.png"
                        style={{ width: "90%", marginBottom: 20 }}
                      />
                    </div>
                  </div>
                )} */}

                <ColdChain cold={cold} />
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default OfferingSection;
