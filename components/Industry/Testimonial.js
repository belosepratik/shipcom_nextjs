import React from "react";
import { Button } from "@material-ui/core";
import Colors from "../../styles/Colors";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import styles from "../../pages/css/FoodAndBeverage.module.css";
import Footer from "../Footer";
import Flickity from "react-flickity-component";
import styless from "../../pages/css/About.module.css";
import GlobalCss from "../../pages/css/Global.module.css";
import {
  MobileNextButton,
  DesktopNextButton,
  MobileLeftButton,
  DesktopPreviousButton,
  RightArrowIcon,
} from "../../assets/Icons/Icon";
import Image from "next/image";
import Link from "next/link";

const Testimonial = () => {
  const matches = useMediaQuery("(max-width:480px)");

  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");

  const [toggleState, setToggleState] = React.useState(1);

  const setToggleHandler = (index) => {
    setToggleState(index);
  };

  const slider_data = [
    {
      title: "Building the modern analytics and BI team",
      desc:
        "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
      img: "/images/work_builder.PNG",
    },
    {
      title: "Building the modern analytics and BI team",
      desc:
        "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
      img: "/images/office_kitchen.PNG",
    },
    {
      title: "Building the modern analytics and BI team",
      desc:
        "Remotely control your farms through a single device. Manage your entire crop life-cycle and produce an optimum harvest.",
      img: "/images/carrier_work.PNG",
    },
  ];

  const flickityOptions = {
    groupCells: true,
    cellAlign: "left",
    pageDots: true,
    lazyLoad: true,
  };

  return (
    <>
      <div
        // className="container-fluid"
        style={{ paddingTop: "5rem" }}
      >
        <div
          className="container-fluid"
          style={{
            width: `${isTabletOrMobileDevice ? "100%" : "80%"} `,
            height: "459px",
            borderRadius: `${isTabletOrMobileDevice ? "0px" : "10px"} `,
            backgroundImage:
              "linear-gradient(rgba(105, 102, 199, 0.85), rgba(105, 102, 199, 0.85)),url('images/stock-photo-welcome-on-board-two-handsome-men-shaking-hands-with-smile-while-sitting-on-the-couch-at-office-407609140.png')",
          }}
        >
          <br />
          <br />
          <br />
          <p
            style={{
              textAlign: "center",
              fontFamily: "Poppins",
              fontWeight: "600",
              fontSize: `${isTabletOrMobileDevice ? "24px" : "34px"}  `,
              color: Colors.white,
            }}
            className={GlobalCss.PoppinsMedium}
          >
            Client testimonials
          </p>
          {isTabletOrMobileDevice ? (
            <div
              className={GlobalCss.PoppinsMedium}
              style={{
                display: "flex",
                overflowY: "scroll",
                paddingTop: "3rem",
              }}
            >
              {toggleState === 1 ? (
                <div
                  style={{
                    borderBottomColor: "#fff",
                    borderBottomStyle: "solid",
                    // padding: "2px",
                    marginRight: 10,
                  }}
                >
                  <p
                    style={{ width: "11rem", color: "#fff" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Harris Health System
                  </p>
                </div>
              ) : (
                <div
                  style={{
                    color: "#FFFFFFCB",
                    marginRight: 10,
                  }}
                  onClick={() => setToggleHandler(1)}
                >
                  <p
                    style={{ width: "11rem", color: "#FFFFFFCB" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Harris Health System
                  </p>
                </div>
              )}
              {toggleState === 2 ? (
                <div
                  style={{
                    borderBottomColor: "#fff",
                    borderBottomStyle: "solid",
                    padding: "2px",
                    marginRight: 10,
                  }}
                >
                  <p
                    style={{ width: "6rem", color: "#fff" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Houston ISD
                  </p>
                </div>
              ) : (
                <div
                  style={{
                    padding: "2px",
                    color: "#FFFFFFCB",
                    marginRight: 10,
                  }}
                  onClick={() => setToggleHandler(2)}
                >
                  <p
                    style={{ width: "6rem", color: "#FFFFFFCB" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Houston ISD
                  </p>
                </div>
              )}
              {toggleState === 3 ? (
                <div
                  style={{
                    borderBottomColor: "#fff",
                    borderBottomStyle: "solid",
                    padding: "2px",
                    marginRight: 10,
                  }}
                >
                  <p
                    style={{ width: "11rem", color: "#fff" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Dermpath Diagnostics
                  </p>
                </div>
              ) : (
                <div
                  style={{
                    padding: "2px",
                    marginRight: 10,
                  }}
                  onClick={() => setToggleHandler(3)}
                >
                  <p
                    style={{ width: "11rem", color: "#FFFFFFCB" }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Dermpath Diagnostics
                  </p>
                </div>
              )}
            </div>
          ) : (
            <div
              style={{
                cursor: "pointer",
                display: "flex",
                justifyContent: "center",
              }}
              className={`container ${GlobalCss.PoppinsRegular}`}
            >
              {/* <div style={{ width: "80%", textAlign: "center" }} class="row"> */}
              {toggleState === 1 ? (
                <div style={{ margin: "1.5rem" }}>
                  <p
                    style={{
                      fontSize: "18px",
                      color: Colors.white,
                      borderBottomColor: "#fff",
                      borderBottomStyle: "solid",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Harris Health System
                  </p>
                </div>
              ) : (
                <div
                  onClick={() => setToggleHandler(1)}
                  style={{ margin: "1.5rem" }}
                >
                  <p
                    style={{
                      fontSize: "18px",
                      color: "#FFFFFF99",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Harris Health System
                  </p>
                </div>
              )}
              {toggleState === 2 ? (
                <div style={{ margin: "1.5rem" }}>
                  <p
                    style={{
                      fontSize: "18px",
                      color: Colors.white,
                      borderBottomColor: "#fff",
                      borderBottomStyle: "solid",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Houston ISD
                  </p>
                </div>
              ) : (
                <div
                  onClick={() => setToggleHandler(2)}
                  style={{ margin: "1.5rem" }}
                >
                  <p
                    style={{
                      fontSize: "18px",
                      color: "#FFFFFF99",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Houston ISD
                  </p>
                </div>
              )}
              {toggleState === 3 ? (
                <div style={{ margin: "1.5rem" }}>
                  <p
                    style={{
                      fontSize: "18px",
                      color: Colors.white,
                      borderBottomColor: "#fff",
                      borderBottomStyle: "solid",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Dermpath Diagnostics
                  </p>
                </div>
              ) : (
                <div
                  onClick={() => setToggleHandler(3)}
                  style={{ margin: "1.5rem" }}
                >
                  <p
                    style={{
                      fontSize: "18px",
                      color: "#FFFFFF99",
                      padding: "2px",
                    }}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  >
                    Dermpath Diagnostics
                  </p>
                </div>
              )}
            </div>
            // </div>
          )}
          <br />
          <br />
          <br />
          <br />
          <div style={{ display: "flex", justifyContent: "center" }}>
            <div
              style={{
                borderRadius: "5px",
                // height: "200px",
                backgroundColor: "#fff",
                boxShadow: "0px 0px 27px #6966C733",
                width: `${isTabletOrMobileDevice ? "90%" : "80%"} `,
                display: "flex",
                // flexDirection: "column",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {isTabletOrMobileDevice ? (
                <MobileLeftButton />
              ) : (
                <div
                  style={{
                    position: "relative",
                    left: "-2.5rem",
                    width: "2rem",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    if (toggleState >= 2) {
                      setToggleState(toggleState - 1);
                    }
                  }}
                >
                  <DesktopPreviousButton />
                </div>
              )}
              <div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div
                    style={{
                      borderRadius: "50%",
                      overflow: "none",
                      boxShadow: "0px 10px 30px #D3D7DE99",
                      position: "relative",
                      top: "-3rem",
                      backgroundColor: "#fff",
                    }}
                  >
                    <img src="images/harris_health.png" />
                  </div>
                </div>
                <p
                  style={{
                    position: "relative",
                    top: "-2rem",
                    textAlign: "center",
                  }}
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                >
                  {toggleState === 1 && (
                    <b className={GlobalCss.PoppinsMedium}>Tim Brown</b>
                  )}
                  {toggleState === 2 && (
                    <b className={GlobalCss.PoppinsMedium}>Conrad Davis</b>
                  )}
                  {toggleState === 3 && (
                    <b className={GlobalCss.PoppinsMedium}>Luis M Soto</b>
                  )}
                  <br />
                  {toggleState === 1 && (
                    <span
                      style={{ color: "#8A92A3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}
                    >
                      Administrative Director, System Logistics
                    </span>
                  )}
                  {toggleState === 2 && (
                    <span
                      style={{ color: "#8A92A3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}
                    >
                      Sr. Warehouse Manager, Nutrition Services
                    </span>
                  )}
                  {toggleState === 3 && (
                    <span
                      style={{ color: "#8A92A3" }}
                      className={GlobalCss.PoppinsMedium + " " + GlobalCss.H14}
                    >
                      Director of Laboratory Operations
                    </span>
                  )}
                </p>
                {toggleState === 1 && (
                  <>
                    <p
                      style={{ textAlign: "center", color: "#383B62",color : "#8A92A3" }}
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      ''The customer service from Shipcom was first class. The
                      entire team was
                      <br /> very engaged with producing a successful solution
                      from grass root
                      <br /> discussions through Go-Live deployment.”
                    </p>
                    {matches ? (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <p style={{ color: "#6966C7", marginBottom: "0px" }}>
                            EDUCATION
                          </p>
                          <p style={{ color: "#EDA343" }}>
                            INVENTORY & WAREHOUSE MANAGEMENT
                          </p>
                        </div>
                      </>
                    ) : (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <span style={{ color: "#6966C7" }}>EDUCATION</span>
                          <span
                            style={{
                              margin: "1px 2%",
                              height: "5px",
                              width: "5px",
                              backgroundColor: "#bbb",
                              borderRadius: "50%",
                              display: "inline-block",
                            }}
                          ></span>
                          <span style={{ color: "#EDA343" }}>
                            INVENTORY & WAREHOUSE MANAGEMENT
                          </span>
                        </div>
                      </>
                    )}
                  </>
                )}
                {toggleState === 2 && (
                  <>
                    <p
                      style={{ textAlign: "center", color: "#383B62",color : "#8A92A3" }}
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      “Shipcom’s Expiration Dashboard helps me to manage the
                      inventory in<br /> way
                       that was not possible before. The [dashboard]
                      allows the operation to be
                      <br /> proactive when dealing with aging material”
                    </p>
                    {matches ? (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <p style={{ color: "#6966C7", marginBottom: "0px" }}>
                            HEALTHCARE
                          </p>
                          <p style={{ color: "#EDA343" }}>LABS MONITORING</p>
                        </div>
                      </>
                    ) : (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <span style={{ color: "#6966C7" }}>HEALTHCARE</span>
                          <span
                            style={{
                              margin: "1px 2%",
                              height: "5px",
                              width: "5px",
                              backgroundColor: "#bbb",
                              borderRadius: "50%",
                              display: "inline-block",
                            }}
                          ></span>
                          <span style={{ color: "#EDA343" }}>
                            LABS MONITORING
                          </span>
                        </div>
                      </>
                    )}
                  </>

                  // <>
                  //   <p
                  //     style={{ textAlign: "center", color: "#383B62"  }}
                  //     className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                  //   >
                  //     “Shipcom’s Expiration Dashboard helps me to manage the
                  //     inventory in way that was not possible before. The
                  //     [dashboard] allows the operation to be proactive when
                  //     dealing with aging material”
                  //   </p>
                  //   {matches ? (
                  //     <>
                  //       <div
                  //         style={{ textAlign: "center", marginBottom: "1rem" }}
                  //         className={
                  //           GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                  //         }
                  //       >
                  //         <p style={{ color: "#6966C7", marginBottom: "0px" }}>
                  //           HEALTHCARE
                  //         </p>
                  //         <p style={{ color: "#EDA343" }}>LABS MONITORING</p>
                  //       </div>
                  //     </>
                  //   ) : (
                  //     <>
                  //       <div
                  //         style={{ textAlign: "center", marginBottom: "1rem" }}
                  //         className={
                  //           GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                  //         }
                  //       >
                  //         <span style={{ color: "#6966C7" }}>HEALTHCARE</span>
                  //         <span
                  //           style={{
                  //             margin: "1px 2%",
                  //             height: "5px",
                  //             width: "5px",
                  //             backgroundColor: "#bbb",
                  //             borderRadius: "50%",
                  //             display: "inline-block",
                  //           }}
                  //         ></span>
                  //         <span style={{ color: "#EDA343" }}>
                  //           LABS MONITORING
                  //         </span>
                  //       </div>{" "}
                  //     </>
                  //   )}
                  // </>
                )}
                {toggleState === 3 && (
                  <>
                    <p
                      style={{ textAlign: "center", color: "#383B62" ,color : "#8A92A3"}}
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      ''The customer service from Shipcom was first class. The
                      entire team was
                      <br /> very engaged with producing a successful solution
                      from grass root
                      <br /> discussions through Go-Live deployment.”
                    </p>

                    {matches ? (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <p style={{ color: "#6966C7", marginBottom: "0px" }}>
                            HEALTHCARE
                          </p>
                          <p style={{ color: "#EDA343" }}>
                            {" "}
                            COLD CHAIN LOGISTICS
                          </p>
                        </div>
                      </>
                    ) : (
                      <>
                        <div
                          style={{ textAlign: "center", marginBottom: "1rem" }}
                          className={
                            GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                          }
                        >
                          <span style={{ color: "#6966C7" }}>HEALTHCARE</span>
                          <span
                            style={{
                              margin: "1px 2%",
                              height: "5px",
                              width: "5px",
                              backgroundColor: "#bbb",
                              borderRadius: "50%",
                              display: "inline-block",
                            }}
                          ></span>
                          <span style={{ color: "#EDA343" }}>
                            COLD CHAIN LOGISTICS
                          </span>
                        </div>
                      </>
                    )}
                  </>
                )}
              </div>
              {isTabletOrMobileDevice ? (
                <MobileNextButton />
              ) : (
                <div
                  style={{
                    position: "relative",
                    width: "2.4rem",
                    marginRight: "5px",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    if (toggleState <= 2) {
                      setToggleState(toggleState + 1);
                    }
                  }}
                >
                  <DesktopNextButton />
                </div>
              )}
            </div>
          </div>
        </div>
        {isTabletOrMobileDevice ? (
          <div style={{ paddingTop: "15rem" }} className="container">
            <div style={{ paddingTop: "5rem", paddingBottom: "3rem" }}>
              <div style={{ textAlign: "center" }}>
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}>
                  Resources
                </p>
                <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}   style={{width :  "69%", margin : "0 auto", color :"#8A92A3"}}>
                Take a deep dive with some of our most relevant case studies, articles, guides, and technical documents.
                  
                </p>
              </div>
            </div>
            <Flickity
              id="carousel"
              className={styles.carousel__main} // default ''
              elementType={"div"} // default 'div'
              options={flickityOptions} // takes flickity options {}
              disableImagesLoaded={false} // default false
              reloadOnUpdate // default false
              static // default false
            >
              {slider_data.map((item, index) => {
                return (
                  <div style={{ paddingTop: "2rem", paddingBottom: "2rem" }}>
                    <div className={styless.carousel__cell}>
                      <div style={{ padding: "2rem" }}>
                        <p
                          className={GlobalCss.PoppinsMedium}
                          style={{ color: "#383B62" }}
                        >
                          {item.title}
                        </p>
                        <p style={{ color: "#8A92A3" }}>{item.desc}</p>
                        <div className={styles.wrapper}>
                          <div className={styles.btn}>
                            <button
                              type="button"
                              style={{
                                background: "none",
                                color: Colors.blue,
                                width: "140px",
                                height: "40px",
                                border: `1px solid ${Colors.blue}`,
                                fontSize: "16px",
                                borderRadius: "4px",
                                transition: "0.6s",
                                overflow: "hidden",
                              }}
                              className={styles.button}
                            >
                              Read more &nbsp; <RightArrowIcon />
                            </button>
                          </div>
                        </div>
                      </div>
                      <div style={{ height: "300px" }}>
                        <Image src={item.img} height={300} width={364}></Image>
                      </div>
                    </div>
                  </div>
                );
              })}
            </Flickity>
          </div>
        ) : (
          <div
            style={{
              paddingTop: `${isTabletOrMobileDevice ? "8rem" : "0rem"}`,
            }}
          >
            <div className="container" id={styles.resources}>
              <div style={{ paddingTop: "15rem", paddingBottom: "3rem" }}>
                <div style={{ textAlign: "center" }}>
                  <p
                  style={{color :"#383B62"}}
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
                  >
                    Resources
                  </p>
                  <p className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{width : `${matches ? "": "46%"}`, margin : "0 auto"}}>
                  Take a deep dive with some of our most relevant case studies, articles, guides, and technical documents.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <div style={{ paddingBottom: "2rem" }}>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      Whitepaper
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H18}
                    >
                      Building the modern analytics and BI team
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      Remotely control your farms through a single device.
                      Manage your entire crop life-cycle and produce an optimum
                      harvest.
                    </p>
                    <a
                      style={{ textDecoration: "none" }}
                      className={
                        GlobalCss.PoppinsSemiBold +
                        " " +
                        GlobalCss.H14 +
                        " " +
                        styles.button
                      }
                    >
                      Learn more
                      <span>
                        <img src="images/Icon ionic-ios-arrow-round-forward-1.png" />
                      </span>
                    </a>
                  </div>
                  <div>
                    <img src="images/MaskGroup84.png" />
                  </div>
                </div>
                <div className="col-md-4">
                  <div style={{ paddingBottom: "2rem" }}>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      Usecase
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H18}
                    >
                      Building the modern analytics and BI team
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      Remotely control your farms through a single device.
                      Manage your entire crop life-cycle and produce an optimum
                      harvest.
                    </p>
                    <a
                      style={{ textDecoration: "none" }}
                      className={
                        GlobalCss.PoppinsSemiBold +
                        " " +
                        GlobalCss.H14 +
                        " " +
                        styles.button
                      }
                    >
                      Learn more
                      <span>
                        <img src="images/Icon ionic-ios-arrow-round-forward-1.png" />
                      </span>
                    </a>
                  </div>
                  <div>
                    <img src="images/Mask Group 85.png" />
                  </div>
                </div>
                <div className="col-md-4">
                  <div style={{ paddingBottom: "2rem" }}>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      Technical document
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H18}
                    >
                      Building the modern analytics and BI team
                    </p>
                    <p
                      className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
                    >
                      Remotely control your farms through a single device.
                      Manage your entire crop life-cycle and produce an optimum
                      harvest.
                    </p>
                    <a
                      style={{ textDecoration: "none" }}
                      className={
                        GlobalCss.PoppinsSemiBold +
                        " " +
                        GlobalCss.H14 +
                        " " +
                        styles.button
                      }
                    >
                      Learn more
                      <span>
                        <img src="images/Icon ionic-ios-arrow-round-forward-1.png" />
                      </span>
                    </a>
                  </div>

                  <div>
                    <img src="images/Mask Group 86.png" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div
          style={{
            // background: "blue",
            // paddingBottom: "10rem",
            textAlign: "center",
            backgroundImage: `url('/images/Rectangle 401.svg')`,
            backgroundRepeat: "repeat",
            // backgroundSize: "600px 900px",
            backgroundSize: "cover",
          }}
          // className="container"
        >
          <div
            style={{
              backgroundImage: `url('/images/Mask Group 100.svg')`,
              backgroundRepeat: "repeat",
              paddingBottom: "10rem",
              // backgroundSize: "600px 900px",
              // backgroundSize: "cover",
              backgroundPosition: "center",
            }}
          >
            <div className="container" style={{ paddingTop: "10rem" }}>
              <p
                style={{ color: "#383B62" }}
                className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
              >
                Let’s work together!
              </p>
              <p
                style={{ color: "#383B62" }}
                className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
              >
                Give us a problem and we’ll find a solution for you
              </p>
              <Link
                style={{ textDecoration: "none", color: "white" }}
                href="/RequestDemo"
              >
                <Button
                  style={{ textTransform: "none" }}
                  color="primary"
                  variant="contained"
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                >
                  Request a demo
                </Button>
              </Link>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Testimonial;
