import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import GlobalCss from "../../pages/css/Global.module.css"

const DescribeSection = ({ content }) => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");
  return (
    <>
      <div
        style={{ paddingTop: "6rem", paddingBottom: "6rem",width:`${isTabletOrMobileDevice ? "90%" : "72%"} ` }}
        className="container"
      >
        <div
          style={{ paddingTop: "1rem", paddingBottom: "1rem" }}
          className="row"
        >
          <div className="col-sm col-md-5">
            <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24} style={{color: "#383B62" }}>
              {content.describe1}
            </p>
          </div>
          <div className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3" }} className="col-sm col-md-7">
            {content.subDescribe1}
          </div>
        </div>
        <hr />
        <div
          style={{ paddingTop: "1rem", paddingBottom: "1rem" }}
          className="row"
        >
          <div className="col-sm col-md-5">
            <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24} style={{color: "#383B62" }}>
              {content.describe2}
            </p>
          </div>
          <div className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3" }} className="col-sm col-md-7">
            {content.subDescribe2}
          </div>
        </div>
        <hr />
        <div
          style={{ paddingTop: "1rem", paddingBottom: "1rem" }}
          className="row"
        >
          <div className="col-sm col-md-5">
            <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H24} style={{ color: "#383B62" }}>
              {content.describe3}
            </p>
          </div>
          <div className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14} style={{ color: "#8A92A3" }} className="col-sm col-md-7">
            {content.subDescribe3}
          </div>
        </div>
      </div>
    </>
  );
};

export default DescribeSection;