import React from "react";
import Colors from "../../styles/Colors";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import GlobalCss from "../../pages/css/Global.module.css";

const HowSection = () => {
  const isTabletOrMobileDevice = useMediaQuery("(max-width:480px)");

  return (
    <div>
      <div style={{ paddingTop: "6rem" }} className="container">
        <p
          style={{
            textAlign: "center",
            color: Colors.lightDenim,
          }}
          className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
        >
          How does Shipcom making
          <br /> buildings smarter?
        </p>

        <p
          style={{
            textAlign: "center",
            color: Colors.gray,
          }}
          className={GlobalCss.PoppinsRegular + " " + GlobalCss.H14}
        >
          We're highlighting how Shipcom is enabling businesses to take
          beneficial <br />
          decisions in the Commercial Real Estate sector
        </p>
        <br />
        <br />
        <br />
        <div class="row">
          <div class="col-sm">
            <div
              style={{
                boxShadow: "0px 10px 30px #D3D7DE99",
                width: `${isTabletOrMobileDevice ? "100%" : "80%"} `,
              }}
            >
              <img
                src="images/stock-photo-two-doctor-wear-ppe-suit-with-face-mask-measure-body-temperature-of-coronavirus-infected-patient-1711001281.png"
                style={{
                  width: "100%",
                  padding: "1rem",
                  position: "relative",
                  top: "-3em",
                }}
              />

              <div
                style={{
                  paddingLeft: "1rem",
                  top: "-2rem",
                  position: "relative",
                }}
              >
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                  Save energy, save money
                </p>
                <p
                  style={{ color: "#8A92A3" }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Take a more informed approach with inventory
                  <br /> management to automatically make re-stocking
                  <br /> decisions for products in greatest demand. Reduce
                  <br />
                  unnecessary expenses through better supply chain
                  <br /> management of short shelf-life items.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm">
            <br />
            <br />
            <br />
            <div
              style={{
                boxShadow: "0px 10px 30px #D3D7DE99",
                width: `${isTabletOrMobileDevice ? "100%" : "80%"} `,
              }}
            >
              <img
                src="images/Mask Group 191.png"
                style={{
                  width: "100%",
                  padding: "1rem",
                  position: "relative",
                  top: "-3em",
                }}
              />

              <div
                style={{
                  paddingLeft: "1rem",
                  top: "-2rem",
                  position: "relative",
                }}
              >
                <p className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}>
                  Data-led decisions
                </p>
                <p
                  style={{ color: "#8A92A3" }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Better forecast demand of food and beverage items
                  <br /> by keeping track of consumption trends ensuring
                  <br /> minimum waste.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div style={{ paddingTop: "3rem" }} class="row">
          <div class="col-sm">
            <div
              style={{
                boxShadow: "0px 10px 30px #D3D7DE99",
                width: `${isTabletOrMobileDevice ? "100%" : "80%"} `,
              }}
            >
              <img
                src="images/Mask Group 189.png"
                style={{
                  width: "100%",
                  padding: "1rem",
                  position: "relative",
                  top: "-3em",
                }}
              />

              <div
                style={{
                  paddingLeft: "1rem",
                  top: "-2rem",
                  position: "relative",
                }}
              >
                <p
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  style={{ fontSize: "18px" }}
                >
                  Increasing safety and security
                </p>
                <p
                  style={{ color: "#8A92A3" }}
                  className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
                >
                  Take a more informed approach with inventory
                  <br /> management to automatically make re-stocking
                  <br /> decisions for products in greatest demand. Reduce
                  <br />
                  unnecessary expenses through better supply chain
                  <br /> management of short shelf-life items.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm">
            <br />
            <br />
            <br />
            <div
              style={{
                boxShadow: "0px 10px 30px #D3D7DE99",
                width: `${isTabletOrMobileDevice ? "100%" : "80%"} `,
              }}
            >
              <img
                src="images/Mask Group 192.png"
                style={{
                  width: "100%",
                  padding: "1rem",
                  position: "relative",
                  top: "-5rem",
                }}
              />

              <div
                style={{
                  paddingLeft: "1rem",
                  top: "-2rem",
                  position: "relative",
                }}
              >
                <p
                  style={{ fontSize: "18px" }}
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                >
                  Timely maintenance
                </p>
                <p
                  className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H18}
                  style={{ color: "#8A92A3" }}
                >
                  Better forecast demand of food and beverage items
                  <br /> by keeping track of consumption trends ensuring
                  <br /> minimum waste.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowSection;
