import Image from "next/image";
import React from "react";
import Colors from "../../styles/Colors";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Button } from "@material-ui/core";
import GlobalCss from "../../pages/css/Global.module.css";

const IntroSection = ({ content }) => {
  const matches = useMediaQuery("(max-width:480px)");

  return (
    <>
      <div style={{ textAlign: "center", paddingTop: "5rem" }}>
        <p
          className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H34}
          style={{
            color: "#383B62",
            width: `${matches ? "81%" : "44%"}`,
            margin: "0 auto",
          }}
        >
          {content.title}
        </p>
        <br />

        <p
          style={{ color: "#8A92A3" }}
          className={GlobalCss.Regular + " " + GlobalCss.H14}
        >
          {content.subTitle}
        </p>

        {matches && (
          <>
            <br />
            <Button
              variant="contained"
              style={{
                textTransform: "none",
                color: "#fff",
                backgroundColor: "#5551C6",
              }}
            >
              <span className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}>
                 
                Request a demo
              </span>
            </Button>
          </>
        )}
      </div>
      <br />
      <br />
      {!matches ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            width: "90%",
            height: "640px",
          }}
          className="container-fluid"
        >
          <img src={content.image} style={{ width: "100%" }} />
        </div>
      ) : (
        <div>
          <img src={content.image} style={{ width: "100%" }} />
        </div>
      )}
    </>
  );
};

export default IntroSection;
