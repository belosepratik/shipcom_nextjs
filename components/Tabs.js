import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Colors from "../styles/Colors";
import styles from "../pages/css/Home.module.css";
import GlobalCss from "../pages/css/Global.module.css";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar
        style={{
          boxShadow: "none",
          WebkitBoxShadow: "none",
          borderBottomColor: "#D3D7DE80",
          borderBottomStyle: "solid",
          borderBottomWidth: "1px",
          //   color: "orange
        }}
        position="static"
        color="white"
      >
        <Tabs
          value={value}
          onChange={handleChange}
          //   indicatorColor="primary"

          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab
            className={[GlobalCss.PoppinsSemiBold, GlobalCss.H14].join(" ")}
            style={{
              color: `${value === 0 ? Colors.orange : "#8A92A3B3"}`,
              textTransform: "none",
            }}
            label="Sensors and tags"
            {...a11yProps(0)}
          />
          <Tab
            className={[GlobalCss.PoppinsSemiBold, GlobalCss.H14].join(" ")}
            style={{
              color: `${value === 1 ? Colors.orange : "#8A92A3B3"}`,
              textTransform: "none",
            }}
            label="Data collection"
            {...a11yProps(1)}
          />
          <Tab
            className={[GlobalCss.PoppinsSemiBold, GlobalCss.H14].join(" ")}
            style={{
              color: `${value === 2 ? Colors.orange : "#8A92A3B3"}`,
              textTransform: "none",
            }}
            label="Data on cloud"
            {...a11yProps(2)}
          />
          <Tab
            className={[GlobalCss.PoppinsSemiBold, GlobalCss.H14].join(" ")}
            style={{
              color: `${value === 3 ? Colors.orange : "#8A92A3B3"}`,
              textTransform: "none",
            }}
            label="Stunning insights"
            {...a11yProps(3)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <br />
        <div className="row" id={styles.iot__product}>
          <div className="col-md-12">
            <img src="images/Layer 2-6.png" style={{ height: "102px" }} />
            <h4
              className={[GlobalCss.PoppinsSemiBold, GlobalCss.H18].join(" ")}
            >
              Sensors and tags
            </h4>
            <span
              className={[GlobalCss.PoppinsRegular, GlobalCss.H14].join(" ")}
            >
              We offer smart sensors, tags, and instructions from all the
              leading manufactures, allowing us to deploy the optimal mix of
              sensory hardware for every Iot Application.
            </span>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <br />
        <div className="row" id={styles.iot__product}>
          <div className="col-md-12">
            <img src="images/Layer2.png" style={{ height: "102px" }} />
            <h4
              className={[GlobalCss.PoppinsSemiBold, GlobalCss.H18].join(" ")}
            >
              Data Collection
            </h4>
            <span
              className={[GlobalCss.PoppinsRegular, GlobalCss.H14].join(" ")}
            >
              Each sensor is programmed to collect specific data through
              gateways which can include parameters like location, temperature,
              air quality, humidity, recent activity & more.
            </span>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <br />
        <div className="row" id={styles.iot__product}>
          <div className="col-md-12">
            <img src="images/Layer 2-4.png" style={{ height: "102px" }} />
            <h4
              className={[GlobalCss.PoppinsSemiBold, GlobalCss.H18].join(" ")}
            >
              Cloud-Based Connectivity
            </h4>
            <span
              className={[GlobalCss.PoppinsRegular, GlobalCss.H14].join(" ")}
            >
              Gateways are connected via the internet through either WiFi or
              Cellular LTE (3G/4G/5G), so all collected sensor data can be
              transferred to the cloud in a secure, reliable network.
            </span>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <br />
        <div className="row" id={styles.iot__product}>
          <div className="col-md-12">
            <img src="images/Layer 2-5.png" style={{ height: "102px" }} />
            <h4 className={[GlobalCss.PoppinsRegular, GlobalCss.H14].join(" ")}>
              Stunning insights
            </h4>
            <span
              className={[GlobalCss.PoppinsRegular, GlobalCss.H14].join(" ")}
            >
              The data is structured to create invaluable insights which can be
              accessed easily through mobile or desktop via
              <span
                className={GlobalCss.PoppinsSemiBold}
                style={{ color: "#6966C7" }}
              >
                Catamaran NextGen™
              </span>
              , our proprietary console.
            </span>
          </div>
        </div>
      </TabPanel>
    </div>
  );
}
