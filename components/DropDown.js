import { ToggleOff } from "@material-ui/icons";
import React from "react";
import styles from "../pages/css/FoodAndBeverage.module.css";
import GlobalCss from "../pages/css/Global.module.css";

const DropDown = ({
  
  items,
  multiSelect = false,
  toggleState,
  setToggleState
}) => {
  const [open, setOpen] = React.useState(false);
  const [selection, setSelection] = React.useState([]);
  const [selected, setSelected] = React.useState(1);

  const toggle = () => setOpen(!open);

  // const handleOnCLick = (item) => {
  //   if (!selection.some((current) => current.id === item.id)) {
  //     if (!multiSelect) {
  //       setSelection([item]);
  //     } else if (multiSelect) {
  //       setSelection([...selection, item]);
  //     }
  //   } else {
  //     let selectionAfterRemoval = selection;
  //     selectionAfterRemoval = selectionAfterRemoval.filter(
  //       (current) => current.id !== item.id
  //     );
  //     setSelection([...selectionAfterRemoval]);
  //   }
  // };

  return (
    <div className={styles.dropdown}>
      <div onClick={() => toggle()} className={styles.dropdown_select}>
        <span
          style={{ color: "#fff", fontFamily : "Poppins", fontWeight : "500", fontSize : "13px" }} 
        >
          {toggleState === 4 && <>Cold chain logistics</>}
          {toggleState === 1 && <> Labs monitoring</>}
          {toggleState === 2 && <> GPS and fleet management</>}
          {toggleState === 3 && <> Occupancy & capacity tracking</>}
        </span>
        <img src="images/Group 20316.svg" />
      </div>
      <div
        style={{
          borderRadius: "4px",
          backgroundColor: "white",
          opacity: `${open ? 1 : 0}`,
          visibility: `${open ? "visible" : "hidden"}`,
          transition: "opacity 0.2s linear, visibility 0.2s linear",
          position: "absolute",
          top: "110%",
          left: 0,
          right: 0,
          backgroundColor:'#21223B'
        }}
      >
        {items.map((item) => (
          <div
            onClick={() => {
              setSelected(item.id);
              setToggleState(item.id);
            }}
            className={styles.dropdown_list_item}
          >
          <span style={{color:"#FFFFFF72"}}>{item.value} </span>  
          </div>
        ))}
      </div>
    </div>
  );
};

export default DropDown;
