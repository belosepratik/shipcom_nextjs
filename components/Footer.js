import { useMediaQuery } from "@material-ui/core";
import React from "react";
import styles from "../components/Footer.module.css";
import GlobalCss from "./../pages/css/Global.module.css";

function Footer() {
  const matches = useMediaQuery("(max-width:480px)");
  return (
    <>
      <div
        className="container-fluid"
        id={styles.Footer}
        className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
      >
        <div className="container">
          <div
            className="row"
            style={{
              width: `${matches && "90%"}`,
              margin: `${matches && "0% auto"}`,
            }}
          >
            <div className="col-md-4">
              <div className="row">
                <div className="col-md-12">
                  <img src="images/Group 150.png" />
                  <p style={{ width: "80%" }}>
                    A leading provider in IoT solutions and single cloud-based
                    applications that helps you grow your business
                    exponentially.
                  </p>
                  <div
                    style={{
                      width: "50%",
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-evenly",
                      paddingBottom: "1rem",
                    }}
                  >
                    <a>
                      <img src="images/Ellipse7.png" />
                      <img
                        src="images/facebook-app-symbol.svg"
                        style={{ marginLeft: "-50%" }}
                      />
                    </a>
                    <a>
                      <img src="images/Ellipse7.png" />
                      <img
                        src="images/Group 20333.svg"
                        style={{ marginLeft: "-50%" }}
                      />
                    </a>
                    <a>
                      <img src="images/Ellipse7.png" />
                      <img
                        src="images/twitter.svg"
                        style={{ marginLeft: "-50%" }}
                      />
                    </a>
                  </div>
                  {matches && <hr />}
                </div>
              </div>
              <div className="row" style={{ paddingTop: "10%" }}>
                <div className="col-md-12">
                  <p
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    Subscribe to our newsletter
                  </p>
                  <p>
                    A monthly digest of the latest IoT news, articles, and
                    resources.
                  </p>
                  <input type="text" placeholder="Email address" style={{padding : matches && "10px 0px" }}/>
                  <button  style={{padding : matches && "14px 10px 12px 10px" }} >Subscribe</button>
                </div>

                {matches && (
                  <div style={{ padding: "10% 0%" }}>
                    <hr />
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-8" style={{ paddingTop: "2%" }}>
              <div className="row">
                <div className="col-6 col-md-3">
                  <p
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    COMPANY
                  </p>
                  <p>About</p>
                  <p>Careers</p>
                  <p>Team</p>
                  <p>Our offices</p>
                </div>

                {matches ? (
                  <div className="col-6 col-md-3">
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      CONTACTUS
                    </p>
                    <p>Request a Demo</p>
                    <p>Job Openings</p>
                  </div>
                ) : (
                  <div className="col-6 col-md-3">
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      OFFERINGS
                    </p>
                    <p>Environmental building monitoring</p>
                    <p>Asset management</p>
                    <p>Inventory and warehouse Management</p>
                    <p>Occupancy and flow management</p>
                    <p>Cold chain Logistics</p>
                    <p>GPS & fleet management</p>
                    <p>labs monitoring</p>
                  </div>
                )}

                <div
                  className="col-6 col-md-3"
                  style={{ paddingTop: `${matches && "5%"}` }}
                >
                  <p
                    className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
                  >
                    INDUSTRY
                  </p>
                  <p>Healthcare</p>
                  <p>Food Supply</p>
                  <p>Agriculture</p>
                  <p>Restaurant</p>
                  <p>Retail</p>
                  <p>Public Sector</p>
                  <p>Education</p>
                  <p>Pharmaceutical</p>
                  <p>Commercial Real Estate</p>
                  <p>Aviation</p>
                  <p>Manufacturing</p>
                </div>

                {matches ? (
                  <div className="col-6 col-md-3" style={{ paddingTop: "5%" }}>
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      OFFERINGS
                    </p>
                    <p>Environmental building monitoring</p>
                    <p>Asset management</p>
                    <p>Inventory and warehouse Management</p>
                    <p>Occupancy and flow management</p>
                    <p>Cold chain Logistics</p>
                    <p>GPS & fleet management</p>
                    <p>labs monitoring</p>
                  </div>
                ) : (
                  <div className="col-6 col-md-3">
                    <p
                      className={
                        GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14
                      }
                    >
                      CONTACTUS
                    </p>
                    <p>Request a Demo</p>
                    <p>Job Openings</p>
                  </div>
                )}

                {matches && (
                  <div className="col-6 col-md-3" style={{ padding: "5% 0% 5% 12px" }}>
                    <p style={{ color: "white" }}>Cookie Policy</p>
                    <p style={{ color: "white" }}>Terms of Use</p>
                    <p style={{ color: "white" }}>Privacy Policy</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="conatiner-fluid"
        id={styles.belowFooter}
        style={{ padding: `${matches && "4% 0%"}` }}
      >
        <div
          className="container"
          className={GlobalCss.PoppinsRegular + " " + GlobalCss.H12}
        >
          <div className="row">
            <div
              className="col-md-6"
              style={{ textAlign: `${matches && "center"}` }}
            >
              <a>Copyright © 2021 Shipcom Wireless IoT, Inc.</a>
            </div>
            {matches ? (
              ""
            ) : (
              <>
                <div className="col-md-2">
                  <a>Cookie Policy</a>
                </div>
                <div className="col-md-2">
                  <a>Terms of Use</a>
                </div>
                <div className="col-md-2">
                  <a>Privacy Policy</a>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default Footer;
