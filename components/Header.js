import Image from "next/image";
import React from "react";
import styles from "./Header.module.css";
import Link from "next/link";
import IndustryContents from "./IndustryContents";
import Colors from "../styles/Colors";
import { Button } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const Header = () => {
  const [showIndustryContent, setShowIndustryContent] = React.useState(false);

  const matches = useMediaQuery("(max-width:480px)");

  return (
    <>
      {showIndustryContent && <IndustryContents />}

      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <Image
            className="navbar-brand"
            width={120}
            height={85}
            src="/images/logo.png"
            style={{ padding: "0% 5%" }}
          />

          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          {/* {matches ? (
            <>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <img
                  src="images/MaskGroup162@3x.png"
                  style={{ width: "40%" }}
                />
                <SwipeableTemporaryDrawer />
              </div>
            </>
          ) : (
            <Header />
          )} */}
          <div className="collapse navbar-collapse" id="navbarText">
            <ul
              className="navbar-nav  me-auto mb-2 mb-lg-0"
              style={{ width: "100%", justifyContent: "flex-end" }}
            >
              <li className="nav-item" id={styles.menu_li}>
                <Link href="/">
                  <a className="nav-link active" aria-current="page">
                    Our offerings
                  </a>
                </Link>
              </li>
              <li className="nav-item" id={styles.menu_li}>
                <a
                  onClick={() => {
                    setShowIndustryContent(!showIndustryContent);
                    console.log("clicked");
                  }}
                  className="nav-link active"
                  aria-current="page"
                  style={{ cursor: "pointer" }}
                >
                  Industry
                </a>
              </li>

              <li className="nav-item" id={styles.menu_li}>
                <Link href="/AboutUs">
                  <a className="nav-link" href="#">
                    About Us
                  </a>
                </Link>
              </li>
              <li className="nav-item" id={styles.menu_li}>
                <Link href="/Career">
                  <a className="nav-link" href="#">
                    Careers
                  </a>
                </Link>
              </li>
              <li className="nav-item" id={styles.menu_li}>
                <Link href="/ContactUs">
                  <a className="nav-link" href="#">
                    Contact Us
                  </a>
                </Link>
              </li>
              <li className="nav-item" id={styles.menu_li}>
                <Link href="/RequestDemo">
                  <a className="nav-link" href="#" id={styles.btn_request}>
                    Request a demo
                  </a>
                </Link>
              </li>
            </ul>

            {/* <span className="navbar-text  ">Navbar </span> */}
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
