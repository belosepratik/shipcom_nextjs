import React from "react";
import { RightArrowIcon } from "../assets/Icons/Icon";
import Flickity from "react-flickity-component";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import styles from "../pages/css/FoodAndBeverage.module.css";
import Colors from "../styles/Colors";
import GlobalCss from "../pages/css/Global.module.css";

const NewsSlider = () => {
  const matches = useMediaQuery("(max-width:480px)");
  const data = [
    {
      label: "NEWS",
      desc:
        "Winners : AIM case study for developing compelling global IoT solutions",
      date: "Dec 12, 2020",
    },
    {
      label: "WHITE PAPER",
      desc: "Helping Harris Health Systems reimagine cold-chain supply",
      date: "Dec 12, 2020",
    },
    {
      label: "BLOG",
      desc:
        "IoT Sensing Technologies can accelerate the safe Distribution of COVID-19 Vaccines",
      date: "Dec 12, 2020",
    },
  ];

  const flickityOptions = {
    groupCells: true,
    cellAlign: "left",
    pageDots: true,
    lazyLoad: true,
  };

  return (
    <>
      <div
        style={{
          display: "flex",
          boxShadow: "0px 10px 26px #D3D7DE5A",
          borderRadius: "5px",
          overflow: "hidden",
          //   maxWidth: "450px",
          flexDirection: "column",
          // padding: `${matches ? "1.5rem" : "0rem"}`,
          //   margin: "1.4rem",
          backgroundColor: Colors.white,
        }}
      >
        <div>
          <img
            src="images/331da53759dd01d644dd990360854eedu.png"
            style={{
              width: `${matches ? "100%" : "140px"} `,
              height: `${matches ? "18rem" : "10rem"}`,
            }}
          />
        </div>
        <div
          style={{
            padding: "10px",
            paddingLeft: "1.5rem",
          }}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div
              className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H12}
              style={{ color: "#6966C7" }}
            >
              BLOG
            </div>
            <span
              style={{
                padding: "3px",
                paddingLeft: "5px",
                paddingRight: "5px",
                borderRadius: "12px",
                backgroundColor: "#FFC477",
                color: Colors.white,
                fontWeight: "600",
              }}
              className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
            >
              New
            </span>
          </div>
          <p
            style={{ color: "#383B62" }}
            className={GlobalCss.PoppinsSemiBold + " " + GlobalCss.H14}
          >
            IoT Sensing Technologies can accelerate the safe Distribution of
            COVID-19 Vaccines
          </p>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <p
              style={{ color: "#8A92A3B3" }}
              className={GlobalCss.PoppinsMedium + " " + GlobalCss.H12}
            >
              Dec 12, 2020
            </p>
            <RightArrowIcon />
          </div>
        </div>
      </div>
    </>
  );
};

export default NewsSlider;
