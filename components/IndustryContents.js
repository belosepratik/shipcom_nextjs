import Link from "next/link";
import React from "react";
import { SmallRightIcon } from "../assets/Icons/Icon";
import Colors from "../styles/Colors";
import styles from "../pages/css/IndustryContent.module.css";

const IndustryContents = () => {
  return (
    <div
      style={{
        backgroundColor: Colors.white,
        position: "absolute",
        boxShadow: "1px 4px 8px 0px rgba(0,0,0,0.22)",
        zIndex: 1000,
        width: "100%",
        marginTop: "5rem",
      }}
      className="container-fluid"
    >
      <div className="row">
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          Agriculture <SmallRightIcon />
          <br />
          <p>Manage your entire farming cycle remotely.</p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          Education <SmallRightIcon />
          <br />
          <p>Safely open campuses by monitoring occupancy.</p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          Manufacturing <SmallRightIcon />
          <br />
          <p>Innovatively collaborate your manufacturing using IoT devices.</p>
        </div>
        {/* <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            borderRadius:"10px"
          }}
          className="col"
        >
          Education <SmallRightIcon />
          <br />
          <p>Safely open campuses by monitoring occupancy.</p>
        </div> */}
        {/* <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            borderRadius:"10px"
          }}
          className="col"
        >
          Manufacturing <SmallRightIcon />
          <br />
          <p>Innovatively collaborate your manufacturing using IoT devices.</p>
        </div> */}
      </div>
      <div className="row">
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          Aviation <SmallRightIcon />
          <br />
          <p>Track baggage, enable cabin climate control and more.</p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          <Link style={{ cursor: "pointer" }} href="/FoodAndBeverage">
            <span style={{ cursor: "pointer" }}>
              Food & Beverage <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p>Get visibility on food quality, production and transportation.</p>
        </div>
        <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          <Link style={{ cursor: "pointer" }} href="/Pharma">
            <span style={{ cursor: "pointer" }}>
              Pharmaceutical &nbsp;
              <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p>
            Create optimal conditions for your drug manufacturing environment.
          </p>
        </div>
        {/* <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            borderRadius:"10px"
          }}
          className="col"
        >
          Aviation <SmallRightIcon />
          <br />
          <p>Track baggage, enable cabin climate control and more.</p>
        </div> */}
        {/* <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            borderRadius:"10px"
          }}
          className="col"
        >
          <Link style={{ cursor: "pointer" }} href="/FoodAndBeverage">
            <span style={{ cursor: "pointer" }}>
              Food & Beverage <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p>Get visibility on food quality, production and transportation.</p>
        </div> */}
        {/* <div
          style={{
            borderBottomColor: "black",
            borderBottomStyle: "solid",
            borderBottomWidth: "1px",
            margin: "2rem",
            borderRadius:"10px"
          }}
          className="col"
        >
          <Link style={{ cursor: "pointer" }} href="/Pharma">
            <span style={{ cursor: "pointer" }}>
              Pharmaceutical &nbsp;
              <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p>
            Create optimal conditions for your drug manufacturing environment.
          </p>
        </div> */}
      </div>
      <div className="row">
        {/* <div
          style={{
            margin: "2rem",
            // backgroundColor: Colors.blue,
            paddingTop: "1rem",
            borderRadius:"10px"
          }}
          className={"col"+" " +styles.col_hover}
        >
          <Link style={{ cursor: "pointer" }} href="/Commercial">
            <span style={{ cursor: "pointer" }}>
              Commercial Real Estate &nbsp; <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p>Automate commercial spaces and make them cost-effective.</p>
        </div> */}
        <div
          style={{
            margin: "2rem",
            paddingTop: "1rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          <Link style={{ cursor: "pointer" }} href="/Commercial">
            <span style={{ cursor: "pointer" }}>
              Commercial Real Estate &nbsp; <SmallRightIcon />
            </span>
          </Link>

          <br />
          <p>Automate commercial spaces and make them cost-effective.</p>
        </div>
        <div
          style={{
            margin: "2rem",
            // backgroundColor: Colors.blue,
            paddingTop: "1rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          <Link style={{ cursor: "pointer" }} href="/HealthCare">
            <span style={{ cursor: "pointer" }}>
              Healthcare &nbsp;
              <SmallRightIcon />
            </span>
          </Link>
          <br />
          <p style={{ color: Colors.black }}>
            Automated solutions for modern healthcare institutions.
          </p>
        </div>
        <div
          style={{
            margin: "2rem",
            paddingTop: "1rem",
            // borderRadius:"10px"
          }}
          className={"col" + " " + styles.col_hover}
        >
          Retail <SmallRightIcon />
          <br />
          <p>Optimize the in-store experience of your shoppers.</p>
        </div>
      </div>
    </div>
  );
};

export default IndustryContents;
